Feature: Prostate Suite

  Background: User is logged in
    Given I open the Guidelines url
    And Empty Patient Record in JSON Payloads

  Scenario: Prostate Cancer Guideline
    And Check the guideline "Prostate" state
#    And Check the guideline status
    Then Change the guideline "Prostate" status Under Test
    And Add "Prostate_bundle"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
#    And Set "Prostate cancer" to "active"
#    And Click the "Next" button
    And I set "Perform DRE to confirm clinical stage" recommendation as "Completed"
    And I set "Perform and/or collect PSAs and calculate PSA density and PSADT" recommendation as "Completed"
    And I set "Obtain and review diagnostic prostate biopsies" recommendation as "Completed"
    And I set "Estimate life expectancy (See Principles of Life Expectancy Estimation [PROS-A])" recommendation as "Completed"
    And I set "Inquire about known high-risk germline mutations" recommendation as "Completed"
    And I set "Obtain family history" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data] Family history criteria to prompt genetic testing" select "Present"
    And In "[Data] Family history of high risk germ-line mutation" select "Present"
#    And Set "[Data Req] Histology prostate cancer" to "Acinar adenocarcinoma"
    And Click the "Next" button
    And I set "Germline testing preferably with pre-test genetic counseling" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data] Germ-line mutation status" select "Present"
    And Click the "Next" button
    And I set "Genetic counseling" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data] Metastatic Stage Value (M)" select "cM0"
    And In "[Data] Clinical Nodal Value (cN)" select "cN0"
    And Click the "Next" button
    And In "[Data] Clinical Tumor Value (cT)" select "cT3a"
    And In "[Data] Primary Gleason pattern" select "Gleason Pattern 5"
    And In "[Data] Gleason grade group" select "Gleason Grade Group 4"
    And In "[Data] PSA level" type "15"
    And Click the "Next" button
    And In "[Data] Probability of lymph node metastasis in decimal point" type "0.2"
    And Click the "Next" button
    And I set "Bone imaging" recommendation as "Completed"
    And I set "Germline testing" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Pelvic CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data] Prostate cancer severity (symptomatic/ asymptomatic)" select "Symptomatic"
    And In "[Data] Life expectancy" select "Life Expectancy Less than Ten Years"
    And Click the "Next" button
    And Select "[PROS-8] RP + PLND" checkbox
    And Click the "Next" button
    And I set "Radical Prostatectomy" recommendation as "Completed"
    And I set "PLND" recommendation as "Completed"
    And Click the "Next" button


