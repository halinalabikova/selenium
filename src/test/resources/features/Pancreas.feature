Feature: Pancreas Suite

  Background: User is logged in
    Given I open the Guidelines url
    And Empty Patient Record in JSON Payloads
    # The same Guideline should be used during the full suite
    And Check the guideline "Pancreas" state
    Then Change the guideline "Pancreas" status Under Test

  Scenario: Pancreas Cancer Guideline Resectable Disease
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "Pancreatic protocol CT (abdomen)" recommendation as "Completed"
    And Click the "Next" button
    And I set "Multidisciplinary consultation" recommendation as "Completed"
    And Click the "Next" button
    And In "[DataReq] cM category" select "cM0"
    And Click the "Next" button
    And I set "Chest and pelvic CT" recommendation as "Completed"
    And Click the "Next" button
    And I set "Liver function test" recommendation as "Completed"
    And Click the "Next" button
    And In "[PANC-2, DataReq] Pancreatic adenocarcinoma resectability status based on multidisciplinary consultation (pancreatic protocol CT)" select "Resectable"
    And Click the "Next" button
    And I set "Consider staging laparoscopy, in high-risk patients or as clinically indicated" recommendation as "Completed"
    And Click the "Next" button
    And Select "?[PANC-2] EUS-guided Biopsy Prior to Neoadjuvant Therapy" checkbox
    And Click the "Next" button
    And I set "EUS-guided biopsy (if considering neoadjuvant therapy)" recommendation as "Completed"
    And Click the "Next" button
    And In "[PANC-7,8 DataReq] ECOG performance status" select "ECOG Performance Status 1"
    And Click the "Next" button
    And Get the Bundle and save it as "test_bundle_k2"

  Scenario: Pancreas Cancer Guideline T1a
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "EUS-guided biopsy (if considering neoadjuvant therapy)" recommendation as "Completed"
    And I set "CBC" recommendation as "Completed"
    And I set "Comprehensive metabolic panel" recommendation as "Completed"
    And I set "Urinalysis" recommendation as "Completed"
    And I set "Chest x-ray" recommendation as "Completed"
    And I set "Bone scan" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Abdominal CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[PANC-7,8 DataReq] ECOG performance status" select "ECOG Performance Status 1"
    And In "[Data Req]cM category" select "cM0"
    And In "[Data Req] cT category" select "cT1a"
    And Click the "Next" button
    # And Select "Ablative Techniques" checkbox
    And Select "Partial nephrectomy (Preferred)" checkbox
    And Click the "Next" button
    And I set "Partial nephrectomy (preferred)" recommendation as "Completed"
    # And I set "Ablative techniques" recommendation as "Completed"
    And Click the "Next" button
    And I set "Surveillance" recommendation as "Completed"
    And Click the "Next" button
    And In "[PANC-2, DataReq] Pancreatic adenocarcinoma resectability status based on multidisciplinary consultation (pancreatic protocol CT)" select "pN0"
    And In "[Data Req] pT category" select "pT1a"
    And Click the "Next" button
    And I set "H&P annually" recommendation as "Completed"
    # And Select "Laboratory tests annually, as clinically indicated" checkbox
    # And Select "Abdominal imaging:? Baseline abdominal CT, MRI, or US within 3–12 mo of surgery ?Then annually for 3 y or longer as clinically indicated" checkbox
    # And Select "Chest imaging ? Chest x-ray or CT Annually for at least 5 y ? Then as Clinically indicated" checkbox
    And Click the "Next" button
    And In "[Data Req] Clinical status of disease" select "Progressive Disease"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And In "[Data req] IMDC prognostic risk group" select "International Metastatic Renal Cell Carcinoma Database Consortium (IMDC) Criteria - Favorable-Risk Group"
    And Click the "Next" button
    And Select "First-line systemic therapy for clear cell histology" checkbox
    And I set "Best supportive care" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button

  Scenario: Pancreas Cancer Guideline T1b
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "H&P" recommendation as "Completed"
    And I set "CBC" recommendation as "Completed"
    And I set "Comprehensive metabolic panel" recommendation as "Completed"
    And I set "Urinalysis" recommendation as "Completed"
    And I set "Chest x-ray" recommendation as "Completed"
    And I set "Bone scan" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Abdominal CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] cN category" select "cN0"
    And In "[Data Req]cM category" select "cM0"
    And In "[Data Req] cT category" select "cT1b"
    And Click the "Next" button
    And Select "Partial Nephrectomy" checkbox
    And Click the "Next" button
    And I set "Partial nephrectomy" recommendation as "Completed"
    And Click the "Next" button
    And I set "Surveillance" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] pN category" select "pN0"
    And In "[Data Req] pT category" select "pT1b"
    And Click the "Next" button
    And I set "H&P annually" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Clinical status of disease" select "Progressive Disease"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And In "[Data req] IMDC prognostic risk group" select "International Metastatic Renal Cell Carcinoma Database Consortium (IMDC) Criteria - Favorable-Risk Group"
    And Click the "Next" button
    And Select "First-line systemic therapy for clear cell histology" checkbox
    And I set "Best supportive care" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button

  Scenario: Pancreas Cancer Guideline T2
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "H&P" recommendation as "Completed"
    And I set "CBC" recommendation as "Completed"
    And I set "Comprehensive metabolic panel" recommendation as "Completed"
    And I set "Urinalysis" recommendation as "Completed"
    And I set "Chest x-ray" recommendation as "Completed"
    And I set "Bone scan" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Abdominal CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] cN category" select "cN0"
    And In "[Data Req]cM category" select "cM0"
    And In "[Data Req] cT category" select "cT2"
    And Click the "Next" button
    And I set "Radical nephrectomy" recommendation as "Completed"
    And Click the "Next" button
    And I set "Surveillance" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] pN category" select "pN0"
    And In "[Data Req] pT category" select "pT2"
    And Click the "Next" button
    And I set "H&P annually" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Clinical status of disease" select "Progressive Disease"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And In "[Data req] IMDC prognostic risk group" select "International Metastatic Renal Cell Carcinoma Database Consortium (IMDC) Criteria - Favorable-Risk Group"
    And Click the "Next" button
    And Select "First-line systemic therapy for clear cell histology" checkbox
    And I set "Best supportive care" recommendation as "Completed"
    And Click the "Next" button

  Scenario: Pancreas Cancer Guideline T3
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "H&P" recommendation as "Completed"
    And I set "CBC" recommendation as "Completed"
    And I set "Comprehensive metabolic panel" recommendation as "Completed"
    And I set "Urinalysis" recommendation as "Completed"
    And I set "Chest x-ray" recommendation as "Completed"
    And I set "Bone scan" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Abdominal CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] cN category" select "cN0"
    And In "[Data Req]cM category" select "cM0"
    And In "[Data Req] cT category" select "cT3"
    And Click the "Next" button
    And I set "Radical nephrectomy" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And I set "Adjuvant sunitinib (category 3)" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] pN category" select "pN0"
    And In "[Data Req] pT category" select "pT3"
    And Click the "Next" button
    And I set "H&P annually" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Clinical status of disease" select "Progressive Disease"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And In "[Data req] IMDC prognostic risk group" select "International Metastatic Renal Cell Carcinoma Database Consortium (IMDC) Criteria - Favorable-Risk Group"
    And Click the "Next" button
    And Select "First-line systemic therapy for clear cell histology" checkbox
    And I set "Best supportive care" recommendation as "Completed"
    And Click the "Next" button

  Scenario: Pancreas Cancer Guideline M1
    Given Add "Pancreas1"
    And Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "H&P" recommendation as "Completed"
    And I set "CBC" recommendation as "Completed"
    And I set "Comprehensive metabolic panel" recommendation as "Completed"
    And I set "Urinalysis" recommendation as "Completed"
    And I set "Chest x-ray" recommendation as "Completed"
    And I set "Bone scan" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Abdominal CT" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] cN category" select "cN0"
    And In "[Data Req]cM category" select "cM1"
    And In "[Data Req] cT category" select "cT3"
    And Click the "Next" button
    And In "[Data Req] Tumor resectability" select "Resectable Carcinoma"
    And Click the "Next" button
    And Click the "Next" button
    And Select "Cytoreductive nephrectomy (in select patients)" checkbox
    And Click the "Next" button
    And I set "Cytoreductive nephrectomy (in select patients)" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Histology" select "Clear Cell Renal Cell Carcinoma"
    And Click the "Next" button
    And In "[Data Req] Number of metastatic sites" select "Limited"
    And In "[Data req] IMDC prognostic risk group" select "International Metastatic Renal Cell Carcinoma Database Consortium (IMDC) Criteria - Intermediate-Risk Group"
    And Click the "Next" button
    And Select "First-line systemic therapy for clear cell histology" checkbox
    And I set "Best supportive care" recommendation as "Completed"
    And Click the "Next" button
    And Click the "Next" button
    And I set "Ipilimumab + nivolumab (category 1) (Preferred)" recommendation as "Completed"
    And Click the "Next" button
    And I set "H&P: Every 6-16 weeks (for patients receiving systemic therapy or more frequently as clinically indicated and adjusted for type of systemic therapy patient is receiving)" recommendation as "Completed"
    And Click the "Next" button
    And In "[Data Req] Clinical status of disease" select "Progressive Disease"
    And Click the "Next" button
    And Select "Subsequent Systemic Therapy for Clear Cell Histology(...)" checkbox
    And Click the "Next" button
    And I set "Nivolumab (category 1, preferred)" recommendation as "Completed"
    And Click the "Next" button



