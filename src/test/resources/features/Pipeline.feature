@CDS-6831 @PIPELINE
Feature: Smoke Tests Suite

  Background: User is logged in
    Given I open the Guidelines url
    And Empty Patient Record in JSON Payloads

  Scenario: Group Request and Negative Request Observation when prompting (Path2)
    And Check the guideline "CDS-6681" state
    Then Change the guideline "CDS-6681" status Under Test
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And Select "None" checkbox
    And Click the "Next" button
    And Select "Some other request" checkbox
    And Set "completed" in line "3"
    And Set "Biopsy" in line "4"
    And Click the "Next" button
    Then "Task2" is Recommended
    And Click the "Next" button
    And Click the "Copy accepted" button
    And Is redirected to "360-oncology" page


  @TEST_CDS-6533 @CDS-6530
  Scenario: DSS Should evaluate expression and prompt only if false
  As an author I sometimes need to prompt for information in certain contexts
  This will enable me to only prompt the user for information when required.
    And Check the guideline "CDS-6530" state
    Then Change the guideline "CDS-6530" status Under Test
    And Patient Record has "cN1", add to JSON Payloads
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    Then "Sentinel node biopsy" is Recommended
    And Empty Patient Record in JSON Payloads
    And Patient Record has "cT1", add to JSON Payloads
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    Then "Sentinel node biopsy" is Recommended
    And Empty Patient Record in JSON Payloads
    And Patient Record has "cN2", add to JSON Payloads
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    Then "ct-category-7th-brca" is Prompted
    And Empty Patient Record in JSON Payloads
    And Patient Record has "cT0", add to JSON Payloads
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    Then "cn-category-7th-brca" is Prompted
    And Patient Record has "cN2"
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    Then There are "no additional recommendations"

  @TEST_CDS-6385 @CDS-6283
  Scenario: Resources generated without status - CDS-6385 ExactlyOne
  In the APP, when a request group is completed (e.g. breast-cancer regimen), the resources referenced by the child actions
  need to be consumed as well so that they are not used further downstream.
    And Check the guideline "CDS-6365-ExactlyOne" state
    Then Change the guideline "CDS-6365-ExactlyOne" status Under Test
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "Lumpectomy - RequestGroup" recommendation as "Completed"
    And Click the "Next" button
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6385 @CDS-6283
  Scenario: Resources generated without status - CDS-6385 AllorNone
  In the APP, when a request group is completed (e.g. breast-cancer regimen), the resources referenced by the child actions
  need to be consumed as well so that they are not used further downstream.
    And Check the guideline "CDS-6365-AllorNone" state
    Then Change the guideline "CDS-6365-AllorNone" status Under Test
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "Lumpectomy - RequestGroup" recommendation as "Completed"
    And Click the "Next" button
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6385 @CDS-6283
  Scenario: Resources generated without status - CDS-6385 All
  In the APP, when a request group is completed (e.g. breast-cancer regimen), the resources referenced by the child actions
  need to be consumed as well so that they are not used further downstream.
    And Check the guideline "CDS-6365-All" state
    Then Change the guideline "CDS-6365-All" status Under Test
    And  Send POST API Request to CDS app services
    And Launch the CDS Hook App
    And I set "Lumpectomy - RequestGroup" recommendation as "Completed"
    And Click the "Next" button
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6306 @CDS-5927
  Scenario: 1.Upload Guideline json file, test the guideline and get patient record - E2E_Test_1
  The purpose of this test case is to verify the Patient Record includes the information provided and the expected recommendations
    And Check the guideline "E2E_Test_1" state
    Then Change the guideline "E2E_Test_1" status Under Test
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    And In "Estrogen Receptor Status" select "Positive"
    And In "Tumor size" type "2"
    And Click the "Next" button
    Then "Sentinel node biopsy" is Recommended
    And "Simple Mastectomy or Lumpectomy" is Recommended
    And I set "Sentinel node biopsy" recommendation as "Completed"
    And Click the "Next" button
    Then "Simple Mastectomy" is Recommended
    And "Lumpectomy" is Recommended
    Then Click the "Back" button
    And Check "Sentinel node biopsy" is still "Completed"
    And Click the "Next" button
    Then "Simple Mastectomy" is Recommended
    And "Lumpectomy" is Recommended
    And Click the "Next" button
    And Click the "Copy accepted" button
    And Is redirected to "360-oncology" page
    Then Get the Patient Record
    And Check "Observation" "Tumor Size" value = "2"
    And Check "Observation" "Estrogen Receptor Status" value = "Positive"
    And Check "Procedure Request" "Simple mastectomy" value = "172043006"
    And Check "Procedure Request" "Sentinel node biopsy" value = "396487001"

  @TEST_CDS-6349 @CDS-6205
  Scenario: Inclusive Pairing Gateway - CDS-6349
  The purpose of this test case is to verify the exclusive merge gateway
    And Check the guideline "CDS-6241" state
    Then Change the guideline "CDS-6241" status Under Test
    # Test 1, C50
    And Patient Record has "C50"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended
    # Test 2, C50 and ER+
    And Patient Record has "ER+"
    And  Send POST API Request to CDS app
    Then "SLNB" is Recommended
    # Test 3, C50 and ER+ and SLNB
    And Patient Record has "SLNB"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended
    And Empty Patient Record in JSON Payloads
    # Test 4, C50 and ER+ and PR+
    And Patient Record has "C50"
    And Patient Record has "ER+"
    And Patient Record has "PR+"
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    # Test 5, C50 and ER+ and PR+ and Mastectomy
    And Patient Record has "Mastectomy"
    And  Send POST API Request to CDS app
    Then "SLNB" is Recommended
    # Test 6, C50 and ER+ and PR+ and Mastectomy and cT category
    And Patient Record has "CT"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    # Test 7, C50 and ER+ and PR+ and Mastectomy and cT category and lumpectomy
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "SLNB" is Recommended
    # Test 8, C50 and ER+ and PR+ and Mastectomy and cT category and lumpectomy and SLNB
    And Patient Record has "SLNB"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended

  @TEST_CDS-6348 @CDS-6205
  Scenario: Recommendations: Logic Tests: 1f One or more - CDS-6348
  This test case is to test recommendation Boundary events
    And Check the guideline "CDS-6240" state
    Then Change the guideline "CDS-6240" status Under Test
    # 1f1 Selection Behaviors One or More
    And Patient Record has "No-resources"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Mastectomy" is Recommended "1" times
    # 1f2 Selection Behaviors One or More
    And Add GuidanceResponse
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Mastectomy" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1f3 Selection Behaviors One or More
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1f4 Selection Behaviors One or More
    And Add GuidanceResponse
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1f5 Selection Behaviors One or More
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy-active"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended "1" times
    Then "Mastectomy" is "Active"
    And Empty Patient Record in JSON Payloads
    # 1f6 Selection Behaviors One or More
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times

  @TEST_CDS-6347 @CDS-6205
  Scenario: Recommendations: Logic Tests: 1d Exactly one - CDS-6347
  This test case is to test recommendation Boundary events
    And Check the guideline "CDS-6239" state
    Then Change the guideline "CDS-6239" status Under Test
    # 1d1 Selection Behaviors Exactly one
    And Patient Record has "No-resources"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Mastectomy" is Recommended "1" times
    # 1d2 Selection Behaviors Exactly one
    And Add GuidanceResponse
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Mastectomy" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1d3 Selection Behaviors Exactly one
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    # 1d4 Selection Behaviors Exactly one
    And Empty Patient Record in JSON Payloads
    And Add GuidanceResponse
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1d5 Selection Behaviors Exactly one
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy-active"
    And  Send POST API Request to CDS app
    Then There are "no recommendation can be provided"
    And Empty Patient Record in JSON Payloads
    # 1d6 Selection Behaviors Exactly one
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy"
    And  Send POST API Request to CDS app
    Then There are "no recommendation can be provided"

  @TEST_CDS-6346 @CDS-6205
  Scenario: Recommendations: Logic Tests: 1e At most one - CDS-6346
  This test case is to test recommendation Boundary events
    And Check the guideline "CDS-6238" state
    Then Change the guideline "CDS-6238" status Under Test
    # 1e1 Selection Behaviors At most one
    And Patient Record has "No-resources"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    Then "Mastectomy" is Recommended "1" times
    # 1e2 Selection Behaviors At most one
    And Add GuidanceResponse
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1e3 Selection Behaviors At most one
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1e4 Selection Behaviors At most one
    And Add GuidanceResponse
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 1e5 Selection Behaviors At most one
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy-active"
    And  Send POST API Request to CDS app
    Then There are "no recommendation can be provided"
    # 1e6 Selection Behaviors At most one
    And Empty Patient Record in JSON Payloads
    And Patient Record has "Lumpectomy"
    And Patient Record has "Mastectomy"
    And  Send POST API Request to CDS app
    Then There are "no recommendation can be provided"

  @TEST_CDS-6343 @CDS-6205
  Scenario: Recommendations: Logic Tests: 1c All or None - CDS-6343
  This test case is to test recommendation Boundary events
    And Check the guideline "CDS-6235" state
    Then Change the guideline "CDS-6235" status Under Test
    # 1c1 Selection Behaviors All or None
    And Patient Record has "No-resources"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Mastectomy" is Recommended
    # 1c2 Selection Behaviors All or None
    And Add GuidanceResponse
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended
    And Empty Patient Record in JSON Payloads
    # 1c3 Selection Behaviors All or None
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # 1c4 Selection Behaviors All or None
    And Add GuidanceResponse
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # 1c5 Selection Behaviors All or None
    And Patient Record has "active" "Mastectomy" and "Lumpectomy", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended
    Then "Mastectomy" is "Active"
    And Empty Patient Record in JSON Payloads
    # 1c6 Selection Behaviors All or None
    And Patient Record has "Lumpectomy" and "Mastectomy", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended

  @TEST_CDS-6344 @CDS-6205
  Scenario: Recommendations: Logic Tests: 1b All - CDS-6344
  This test case is to test recommendation Boundary events
    And Check the guideline "CDS-6233" state
    Then Change the guideline "CDS-6233" status Under Test
    # 1b1 Selection Behaviors All
    And Patient Record has "No-resources", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Mastectomy" is Recommended
    # 1b2 Selection Behaviors All
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended
    # 1b3 Selection Behaviors All
    And Add GuidanceResponse
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "completed"
    Then "Mastectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # 1b4 Selection Behaviors All
    And Patient Record has "active" "Mastectomy" and "Lumpectomy", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended
    Then "Lumpectomy" is "Completed"
    Then "Mastectomy" is Recommended
    Then "Mastectomy" is "Active"
    And Empty Patient Record in JSON Payloads
    # 1b5 Selection Behaviors All
    And Patient Record has "Lumpectomy" and "Mastectomy", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended


  @TEST_CDS-6345 @CDS-6205
  Scenario: Context: Context On Recommendations - CDS-6345
  The purpose of this test case is to verify Context on recommendations
    And Check the guideline "CDS-6231" state
    Then Change the guideline "CDS-6231" status Under Test
    # 2b1 Context On Prompts
    And Patient Record has "Lumpectomy"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended "1" times
    And Empty Patient Record in JSON Payloads
    # 2b2 Context On Prompts
    And Patient Record has "Lumpectomy-With-Encounter"
    And Patient Record has "Work-up"
    And  Send POST API Request to CDS app
    Then There are "Prompts"
    Then There are "cT"
    # 2b3 Context On Prompts
    And Patient Record has "CT"
    And  Send POST API Request to CDS app
    Then There are "Prompts"
    Then There are "cT"
    And Empty Patient Record in JSON Payloads
    # 2b4 Context On Prompts
    And Patient Record has "Lumpectomy-With-Encounter"
    And Patient Record has "Work-up"
    And Patient Record has "cT-With-Encounter"
    And Patient Record has "Work-up"
    And  Send POST API Request to CDS app
    Then "ROB" is Recommended "1" times

  @TEST_CDS-6342 @CDS-6205
  Scenario: Parallel Split Gateway - CDS-6342
  The purpose of this test case is to verify the parallel split gateway
    And Check the guideline "CDS-6229" state
    Then Change the guideline "CDS-6229" status Under Test
    # Sequence flow split
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    And "Lumpectomy" is Recommended

  @TEST_CDS-6341 @CDS-6205
  Scenario: Parallel Merge Gateway - CDS-6341
  The purpose of this test case is to verify the parallel merge gateway
    And Check the guideline "CDS-6228" state
    Then Change the guideline "CDS-6228" status Under Test
    # Condition not true, No recommendation
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then No additional Recommendations
    # Top condition true, Task recommended
    And Patient Record has "C50" and "ER+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended

  @TEST_CDS-6340 @CDS-6205
  Scenario: Inclusive Split Gateway - CDS-6340
  The purpose of this test case is to verify the inclusive split gateway
    And Check the guideline "CDS-6227" state
    Then Change the guideline "CDS-6227" status Under Test
    # Neither condition true, No recommendation
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then No additional Recommendations
    # Top condition true, Top task recommended
    And Patient Record has "C50" and "ER+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    # Both conditions true, Both tasks recommended
    And Patient Record has "C50" and "ER+" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    Then "Lumpectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # Bottom condition true, Bottom task recommended
    And Patient Record has "C50" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6339 @CDS-6205
  Scenario: Inclusive Merge Gateway - CDS-6339
  The purpose of this test case is to verify the inclusive merge gateway
    And Check the guideline "CDS-6226" state
    Then Change the guideline "CDS-6226" status Under Test
    # Top condition true, Task recommended once
    And Patient Record has "C50" and "ER+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    # Both conditions true, Task recommended once
    And Patient Record has "C50" and "ER+" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # Bottom condition true, Task recommended once
    And Patient Record has "C50" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended

  @TEST_CDS-6338 @CDS-6205
  Scenario: Exclusive Split Gateway - CDS-6338
  The purpose of this test case is to verify the exclusive split gateway
    And Check the guideline "CDS-6224" state
    Then Change the guideline "CDS-6224" status Under Test
    # Neither condition true, No recommendation
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then No additional Recommendations
    # Top condition true, Top task recommended
    And Patient Record has "C50" and "ER+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    # Both conditions true, 1st evaluated (top) recommended
    And Patient Record has "C50" and "ER+" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    And Empty Patient Record in JSON Payloads
    # Bottom condition true, Bottom task recommended
    Given Patient Record has "C50" and "PR+", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6337 @CDS-6205
  Scenario: Exclusive Merge Gateway - CDS-6337
  The purpose of this test case is to verify the exclusive merge gateway
    And Check the guideline "CDS-6223" state
    Then Change the guideline "CDS-6223" status Under Test
    # Condition not true, Task recommended once
    And Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app
    Then "Mastectomy" is Recommended
    # Top condition true, Task recommended twice
    And Patient Record has "ER+"
    And  Send POST API Request to CDS app
    And "Mastectomy" is Recommended "2" times

  @TEST_CDS-6336 @CDS-6205
  Scenario: Loop backs Logic Tests: 1b Loop back through tasks - CDS-6336
  The purpose of this test case is to Loop back through task groups
    And Check the guideline "CDS-6183" state
    Then Change the guideline "CDS-6183" status Under Test

#  Scenario: 1b1 Loop back through task groups
    Given Patient Record has "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    When Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy

#  Scenario: 1b2 Loop back through task groups
    Given Patient Record has "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    When Launch the CDS Hook App
    Then "Lumpectomy" is Recommended
    And Click the "Next" button
    Then "Lumpectomy" is Recommended
    And I set "Lumpectomy" recommendation as "Completed"
    And  Click the "Next" button
    Then "ROB" is Recommended

  @TEST_CDS-6334 @CDS-6205
  Scenario: Expressions Logic Tests: 1d SelectionBehaviours: Including parent and child interactions - CDS-6334
  The purpose of this test case is to verify Expressions: SelectionBehaviours: (Including parent and child interactions): At most 1, None of the following
    And Check the guideline "CDS-6182" state
    Then Change the guideline "CDS-6182" status Under Test

#  Scenario: 1d1 SelectionBehaviours :At most 1, None of the following
    Given Patient Record has "CT", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

#  Scenario: 1d2 SelectionBehaviours :At most 1, None of the following
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

#  Scenario: 1d3 SelectionBehaviours :At most 1, None of the following
    Given  Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

#  Scenario: 1d4 SelectionBehaviours :At most 1, None of the following
    Given Patient Record has "ER" and "PR", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6335 @CDS-6205
  Scenario: Loop backs Logic Tests: 1a Loop back through tasks - CDS-6335
  The purpose of this test case is to verify Loop back through tasks
    And Check the guideline "CDS-6181" state
    Then Change the guideline "CDS-6181" status Under Test

#  Scenario: 1a1 Loop back through tasks
    Given Patient Record has "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    When Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy

#  Scenario: 1a2 Loop back through tasks
    Given Patient Record has "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    When Launch the CDS Hook App
    Then "Lumpectomy" is Recommended
    And I set "Lumpectomy" recommendation as "Completed"
    And  Click the "Next" button
    Then "ROB" is Recommended

  @TEST_CDS-6333 @CDS-6205
  Scenario: Context: Context On Prompts - CDS-6333
  The purpose of this test case is to verify Context on prompts
    And Check the guideline "CDS-6179" state
    Then Change the guideline "CDS-6179" status Under Test
    # 1b1 Context On Prompts
    And Patient Record has "CT"
    And  Send POST API Request to CDS app
    Then There are "Prompts"
    Then There are "cT"
    And Empty Patient Record in JSON Payloads
    # 1b2 Context On Prompts
    And Patient Record has "cT-With-Encounter"
    And Patient Record has "Work-up"
    And  Send POST API Request to CDS app
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6332 @CDS-6205
  Scenario: Expressions Logic Tests: 1c SelectionBehaviours: Including parent and child interactions - CDS-6332
  The purpose of this test case is to verify Expressions: SelectionBehaviours: (Including parent and child interactions): At least 2
    And Check the guideline "CDS-6174" state
    Then Change the guideline "CDS-6174" status Under Test

#  Scenario: 1c1 SelectionBehaviours :At least 2
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

#  Scenario: 1c2 SelectionBehaviours :At least 2
    Given Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6331 @CDS-6205
  Scenario: Expressions Logic Tests: 1b SelectionBehaviours: Including parent and child interactions - CDS-6331
  The purpose of this test case is to verify Expressions: SelectionBehaviours: (Including parent and child interactions): and/or, ALL of the following
    And Check the guideline "CDS-6173" state
    Then Change the guideline "CDS-6173" status Under Test

#  Scenario: 1b1 SelectionBehaviours :and/or, ALL of the following
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

#  Scenario: 1b2 SelectionBehaviours :and/or, ALL of the following
    Given Patient Record has "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

#  Scenario: 1b3 SelectionBehaviours :and/or, ALL of the following
    Given Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6330 @CDS-6205
  Scenario: Expressions Logic Tests: 1a SelectionBehaviours: Including parent and child interactions - CDS-6330
  The purpose of this test case is to verify Expressions: SelectionBehaviours
    And Check the guideline "CDS-6171" state
    Then Change the guideline "CDS-6171" status Under Test

#  Scenario: 1a1 SelectionBehaviours :or, and
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

#  Scenario: 1a2 SelectionBehaviours :or, and
    Given Patient Record has "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

#  Scenario: 1a3 SelectionBehaviours :or, and
    Given Patient Record has "ER" and "PR", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6329 @CDS-6205
  Scenario: Recommendations Logic Tests: 3a Boundary events - CDS-6329
  The purpose of this test case is to verify recommendation Boundary events
    And Check the guideline "CDS-6166" state
    Then Change the guideline "CDS-6166" status Under Test

#  Scenario: 3a1 Boundary events
    Given Patient Record has "active" "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy

#  Scenario: 3a2 Boundary events
    Given Patient Record has "active" "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered,Recommend Lumpectomy and Mastectomy, Recommend ROB

#  Scenario: 3a3 Boundary events
    Given Patient Record has "Lumpectomy" and "Mastectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then No additional Recommendations

# need add smoke test cases aa well
  @TEST_CDS-6328 @CDS-6205
  Scenario: Recommendations Logic Tests: 2a Task groups - CDS-6328
  The purpose of this test case is to verify recommendation task group
    And Check the guideline "CDS-6165" state
    Then Change the guideline "CDS-6165" status Under Test

#  Scenario: 2a1 Task groups
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy

#  Scenario: 2a2 Task groups
    Given Patient Record has "C50" and "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "ROB" is Recommended

#  Scenario: 2a3 Task groups
    Given Patient Record has "active" "Mastectomy" and "C50" and "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Off guideline message

  @TEST_CDS-6327 @CDS-6205
  Scenario: Recommendations Logic Tests: 1a SelectionBehaviours - CDS-6327
  The purpose of this test case is to verify recommendation selection behavior
    And Check the guideline "CDS-6164" state
    Then Change the guideline "CDS-6164" status Under Test

#  Scenario: 1a1 Selection Behaviours: Any
    Given Empty Patient Record in JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy

#  Scenario: 1a2 Selection Behaviours: Any
    Given  Patient Record no Resources and has GuidanceResponse
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered with GuidanceResponse, Recommend ROB

#  Scenario: 1a3 Selection Behaviours: Any
    Given Patient Record has "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "ROB" is Recommended

#  Scenario: 1a4 Selection Behaviours: Any
    Given Patient Record has "active" "Mastectomy" and "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Completed Lumpectomy, Active Mastectomy

  @TEST_CDS-6326 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 4c Upload Guideline and set its status - CDS-6326
  The purpose of this test case is to verify Expression on DataRequirements: Expressions for all Child DataRequirements not satisfied
    And Check the guideline "CDS-6163" state
    Then Change the guideline "CDS-6163" status Under Test

#  Scenario: 4c1 Expression on DataRequirements:Expressions for all Child DataRequirements not satisfied
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for cT

#  Scenario: 4c2 Expression on DataRequirements:Expressions for all Child DataRequirements not satisfied
    Given Patient Record has "C50" and "PR", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER, cT

  @TEST_CDS-6325 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 4b Upload Guideline and set its status - CDS-6325
  The purpose of this test case is to verify Expression on DataRequirements: Expressions for one parent DataRequirement not satisfied
    And Check the guideline "CDS-6124" state
    Then Change the guideline "CDS-6124" status Under Test

#  Scenario: 4b1 Expression on DataRequirements:Expressions for one parent DataRequirement not satisfied
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for cT

#  Scenario: 4b2 Expression on DataRequirements:Expressions for one parent DataRequirement not satisfied
    Given Patient Record has "C50" and "PR", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER, cT

  @TEST_CDS-6324 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 4a Upload Guideline and set its status - CDS-6324
  The purpose of this test case is to verify Expression on DataRequirements:Expression for Prompt is not satisfied
    Given I open the Guidelines url
    And Check the guideline "CDS-6122" state
    Then Change the guideline "CDS-6122" status Under Test

#  Scenario: 4a1 Expression on DataRequirements:Expression for Prompt is not satisfied
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

#  Scenario: 4a2 Expression on DataRequirements:Expression for Prompt is not satisfied
    Given Patient Record has "C50" and "PR", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER

  @TEST_CDS-6323 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 3a Upload Guideline and set its status - CDS-6323
  The purpose of this test case is to verify DataRequirements on tasks
    And Check the guideline "CDS-6121" state
    Then Change the guideline "CDS-6121" status Under Test

#  Scenario: 3a1 When set on tasks
    Given Patient Record has "Lumpectomy", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for C50

#  Scenario: 3a2 When set on tasks
    Given Patient Record has "cancelled" "ROB", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER

  @TEST_CDS-6322 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 2c Upload Guideline and set its status - CDS-6322
  The purpose of this test case is to verify SelectionBehaviours(At least 2) of DataRequirements
    And Check the guideline "CDS-6102" state
    Then Change the guideline "CDS-6102" status Under Test

#  Scenario: 2c1 Selection Behaviours: At least 2
    Given Empty Patient Record in JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered,Prompt for C50, ER, PR

#  Scenario: 2c2 Selection Behaviours: At least 2
    Given  Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER, PR

#  Scenario: 2c3 Selection Behaviours: At least 2
    Given  Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6321 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 2b Upload Guideline and set its status - CDS-6321
  The purpose of this test case is to verify SelectionBehaviours(and/or, ALL of the following) of DataRequirements
    And Check the guideline "CDS-6101" state
    Then Change the guideline "CDS-6101" status Under Test

#   Scenario: 2b1 Selection Behaviours: and/or, ALL of the following
    Given Empty Patient Record in JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered,Prompt for C50, ER, PR

#  Scenario: 2b2 Selection Behaviours: and/or, ALL of the following
    Given Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for ER, PR

#  Scenario: 2b3 Selection Behaviours: and/or, ALL of the following
    Given Patient Record has "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for C50

#  Scenario: 2b4 Selection Behaviours: and/or, ALL of the following
    Given  Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6320 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 2a Upload Guideline and set its status - CDS-6320
  The purpose of this test case is to verify SelectionBehaviours(or, and) of DataRequirements
    And Check the guideline "CDS-6094" state
    Then Change the guideline "CDS-6094" status Under Test

#  Scenario: 2a1 Selection Behaviours: or, and
    Given Empty Patient Record in JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered,Prompt for C50, ER, PR

#  Scenario: 2a2 Selection Behaviours: or, and
    Given  Patient Record has "C50", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

#  Scenario: 2a3 Selection Behaviours: or, and
    Given  Patient Record has "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Prompt for C50, PR

#  Scenario: 2a4 Selection Behaviours: or, and
    Given Patient Record has "C50" and "ER", add to JSON Payloads
    And Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then "Lumpectomy" is Recommended

  @TEST_CDS-6319 @CDS-6205
  Scenario: DataRequirements and Prompts Logic Tests: 1a Upload Guideline and set its status - CDS-6319
  The purpose of this test case is to verify DataRequirements and prompts for logic tests
    And Check the guideline "CDS-6087" state
    Then Change the guideline "CDS-6087" status Under Test

#  Scenario: 1a1 Main Start Node test
    Given Patient Record has "C50", add to JSON Payloads
    And  Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is not triggered

#  Scenario: 1a2 Main Start Node test
    Given Patient Record has "C50" and "ER", add to JSON Payloads
    And  Send POST API Request to CDS app services
    Then Launch the CDS Hook App
    Then Guideline is triggered, Don't Prompt, Recommend Lumpectomy
