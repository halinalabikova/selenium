package com.varian.cdsTest.runner;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

import static com.varian.autotest.cds.managers.FileReaderManager.getConfigReader;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"com.varian.cdsTest.stepDefinitions"},
        plugin = {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:target/test-reports/report.html", "json:target/test-reports/report.json"}, //The pipeline looks for the test-reports directory to show the report
        tags = {"@PIPELINE"},
        monochrome = true
)
public class TestRunner {
    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(new File(getConfigReader().getReportConfigPath()));
        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Machine", 	System.getProperty("os.name"));
        Reporter.setSystemInfo("Java Version", System.getProperty("java.version"));
        Reporter.setSystemInfo("Selenium", "3.14.0");
        Reporter.setSystemInfo("Gradle", "3.0");
    }
}