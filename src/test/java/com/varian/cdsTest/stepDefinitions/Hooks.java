package com.varian.cdsTest.stepDefinitions;

import static org.testng.Assert.assertTrue;
import static com.varian.autotest.cds.Index.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.google.common.io.Files;
import com.varian.autotest.cds.cucumber.TestContext;
import com.varian.autotest.cds.logging.Log;
import com.varian.autotest.cds.pageObjects.GuidelinePage;
import com.varian.autotest.cds.pageObjects.LoginPage;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import static com.varian.autotest.cds.managers.FileReaderManager.getConfigReader;

public class Hooks {
    private TestContext testContext;
    private LoginPage loginPage;
    private GuidelinePage guidelinePage;

    public Hooks(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
        guidelinePage = testContext.getPageObjectManager().getGuidelinePage();
    }

    @Before(order = 2)
    public void loginAndGetCookie() {
        loginPage.navigateTo(getConfigReader().getDeploymentUrl() + GUIDELINE_LIST);
        String currentUrl = loginPage.getCurrentUrl();
            Log.info("Updating cookie");
            loginPage.deleteCookies();
            loginPage.vaisLogin();
            loginPage.vaisLoginUsername();
            loginPage.vaisLoginPassword();
            loginPage.azureStaySignInButtonNo();
            loginPage.saveCookies();
    }

    /**
     * Delete all cookies at the start of each scenario to avoid
     * shared state between tests
     */
    @Before(order = 1)
    public void deleteAllCookies() {
        testContext.getDriverManager().getDriver().manage().deleteAllCookies();
    }

    @Before(order = 0)
    public void logScenarioStart(Scenario scenario) {
        Log.startTestCase(scenario.getName());
    }

    /**
     * Embed a screenshot in test report if test is marked as failed
     */
    @After(order = 1)
    public void logScenarioEnd(Scenario scenario) {
        Log.endTestCase(scenario.getName());
    }

    @After(order = 1)
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            String screenshotName = scenario.getName().replaceAll(" ", "_");
            try {
                //This takes a screenshot from the driver at save it to the specified location
                File sourcePath = ((TakesScreenshot) testContext.getDriverManager().getDriver()).getScreenshotAs(OutputType.FILE);

                //Building up the destination path for the screenshot to save
                //Also make sure to create a folder 'screenshots' with in the cucumber-report folder
                File destinationPath = new File(System.getProperty("user.dir") + "/target/test-reports/screenshots/" + screenshotName + ".png");

                //Copy taken screenshot from source location to destination location
                Files.copy(sourcePath, destinationPath);

                //This attach the specified screenshot to the test
                Reporter.addScreenCaptureFromPath(destinationPath.toString());
            } catch (IOException e) {
                Log.error("Creating screenshot failed", e);
            }
        }
    }

    @After(order = 0)
    public void AfterSteps() throws IOException {
        /*Delete the Guideline
         */
        loginPage.openGuidelineListURL();
//        Log.info("The guideline is: " + guidelinePage.guidelineName);
//        if (guidelinePage.verifyGuidelineIsCreated(guidelinePage.guidelineName)) {
//            if (getConfigReader().getPipeline()){
//                Boolean guidelineDraft = guidelinePage.setGuidelineStatusToDraft(guidelinePage.guidelineName);
//                assertTrue(guidelineDraft,"The Guideline was set to Draft");
//        }
//            else {
//                Boolean guidelineDeleted = guidelinePage.deleteUploadGuideline(guidelinePage.guidelineName);
//                assertTrue(guidelineDeleted, "The Guideline was not deleted");
//            }
//        }
//        else {
//            Log.info("The Guideline " + guidelinePage.guidelineName + " was not created, closing the driver");
//        }
        /*Stop the Driver
         */
        testContext.getDriverManager().closeDriver();

    }
}
