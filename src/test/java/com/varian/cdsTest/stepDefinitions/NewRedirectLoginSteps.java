package com.varian.cdsTest.stepDefinitions;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.testng.Assert.assertTrue;

import com.varian.autotest.cds.cucumber.TestContext;
import com.varian.autotest.cds.pageObjects.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class NewRedirectLoginSteps {

    private TestContext testContext;
    private LoginPage loginPage;

    public NewRedirectLoginSteps(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
    }

    @Given("^I open the secure guideline login website$")
    public void openSecureGuidelineLoginWebsiteAzure() {
        //loginPage.openGuidelineUrlAzure();
        loginPage.waitForPageLoad();
        assertThat(loginPage.currentUrl(), containsString(loginPage.azureRedirectUrlValue()));
        loginPage.azureSignIn();
        loginPage.azurePassword();
        loginPage.azureStaySignInButtonNo();
    }

}