package com.varian.cdsTest.stepDefinitions;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.varian.autotest.cds.cucumber.TestContext;
import com.varian.autotest.cds.logging.Log;
import com.varian.autotest.cds.pageObjects.CDSHookAppPage;
import com.varian.autotest.cds.pageObjects.E2eRecommendationsPage;
import com.varian.autotest.cds.pageObjects.GuidelinePage;
import com.varian.autotest.cds.pageObjects.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.minidev.json.JSONArray;

import java.io.IOException;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EndToEndTest1Steps {
    private TestContext testContext;
    private LoginPage loginPage;
    private GuidelinePage guidelinePage;
    private E2eRecommendationsPage e2eRecommendationsPage;
    private CDSHookAppPage cdsHookAppPage;

    private String guidelineJsonFileName ;
    private static String cdsHookAppUrl;
    private String patientRecord;
    private JSONArray valueFromJson;
    private String jsonPathTumorSize = "$.decisions.[*].create[?(@.resourceType == \"Observation\")].valueQuantity.value";
    private String jsonPathEstrogenReceptor = "$.decisions.[*].create[?(@.resourceType == \"Observation\")].valueCodeableConcept.coding[*].display";
    private String jsonPathSimpleMastectomy = "$.decisions.[*].create[?(@.resourceType == \"ProcedureRequest\")].code.coding[?(@.code==\"172043006\")].code";
    private String jsonPathSentinelNodeBiopsy = "$.decisions.[*].create[?(@.resourceType == \"ProcedureRequest\")].code.coding[?(@.code==\"396487001\")].code";
    private String jsonPathPatientPathUrl = "$.decisions.[*].create[?(@.resourceType == \"DocumentReference\")].content[*].attachment.url";
    private String jsonPathContextReference = "$.decisions.[*].create[?(@.resourceType == \"Observation\")].context.reference";
    private String mainWindow;


    public EndToEndTest1Steps(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
        guidelinePage = testContext.getPageObjectManager().getGuidelinePage();
        e2eRecommendationsPage = testContext.getPageObjectManager().getE2eRecommendationsPage();
        cdsHookAppPage = testContext.getPageObjectManager().getCDSHookAppPage();
    }

    @And("^I upload the \"([^\"]*)\" json file$")
    public void uploadTheJsonFile(String fileName) throws IOException {
        guidelineJsonFileName = fileName;
        Log.info("When Upload " + guidelineJsonFileName + " json file");
        assertTrue(guidelinePage.isGuidelineUploadButtonDisplay(), "The Upload Display button was not available");
        guidelinePage.onUploadAGuidelineJsonFile(guidelineJsonFileName);
    }

    @And("^I set the the guideline status to Under Test")
    public void setGuidelineStatus() {
        Log.info("And new guideline is created");
        assertNotNull(guidelineJsonFileName, "Guideline JSON file is not null");
        Log.info("Then Set the guideline status Under Test");
        assertTrue(guidelinePage.verifyGuidelineIsCreated(guidelineJsonFileName), "The Guideline has not been created");
        Boolean guidelineUndertest = guidelinePage.setGuidelineStatusToUnderTest(guidelineJsonFileName);
        assertTrue(guidelineUndertest, "The Guideline is not under test");
    }

    @And("^I send POST API Request with \"([^\"]*)\" in the body$")
    public void externalAPIcall2(String jsonFile) throws IOException {
        Log.info("API call to json file " + jsonFile);
        cdsHookAppPage.getModifiedHookInstanceValueInJson(jsonFile);
        cdsHookAppUrl = cdsHookAppPage.sendPostToCdsHookApp();
    }

    @And("^I launch the CDS Hook App$")
    public void launchCdsHookApp (){
        Log.info("The url from previous step is: " + cdsHookAppUrl);
        e2eRecommendationsPage.openPromptsPageURL(cdsHookAppUrl);
        assertTrue(e2eRecommendationsPage.isPromptsPageOpen(), "The Prompts page is not available");
    }

    @And("^Set Estrogen Receptor Status as \"([^\"]*)\"$")
    public void runTestOnCDSHookApp(String status){
        assertTrue(e2eRecommendationsPage.isEstrogenReceptorStatusInput(), "The Estrogen Receptor Status input line is not available");
        e2eRecommendationsPage.setPromptStatus(status);
    }

    @And("^Set Tumor Size (\\d+)$")
    public void runTestOnCDSHookApp(int tumorSize){
        assertTrue(e2eRecommendationsPage.isTumorSizeCheckbox(), "The Tumor size Checkbox is not available");
        assertTrue(e2eRecommendationsPage.isTumorSizeInput(), "The Tumor Size input line is not available");
        e2eRecommendationsPage.setPromptSize(tumorSize);
    }

    @And("^Only Set Tumor Size (\\d+)$")
    public void tumorSizeOnly(int tumorSize){
        assertTrue(e2eRecommendationsPage.isTumorSizeCheckbox(), "The Tumor size Checkbox is not available");
        assertTrue(e2eRecommendationsPage.isOnlyTumorSizeInput(), "The Tumor Size input line is not available");
        e2eRecommendationsPage.setAlonePromptSize(tumorSize);
    }

    @And("^Set \"([^\"]*)\"$")
    public void setInputData(String dataInput){
        assertTrue(e2eRecommendationsPage.isEstrogenReceptorStatusInput(), "Input line is not available");
        e2eRecommendationsPage.setMultiplePromptStatus(dataInput);
    }

    @And("^Set \"([^\"]*)\" to \"([^\"]*)\"$")
    public void setMultipleInputData(String dataInputAttribute, String dataInputValue){
        e2eRecommendationsPage.waitForPageLoad();
        if (dataInputAttribute.equals("Estrogen Receptor Status")){
            assertTrue(e2eRecommendationsPage.isTumorSizeInput(), "Input line is not available");
            e2eRecommendationsPage.setMultiplePromptStatus(dataInputValue);
        }
        if (dataInputAttribute.equals("Tumor Size")){
            assertTrue(e2eRecommendationsPage.isOnlyTumorSizeInput(), "Input line is not available");
            e2eRecommendationsPage.setDataInput2(dataInputValue);
        }
    }

    @And("^In \"([^\"]*)\" select \"([^\"]*)\"$")
    public void selectValue(String prompt, String value){
        e2eRecommendationsPage.waitForPageLoad();
        Log.info("Prompt options: " + e2eRecommendationsPage.getPromptNames());
        assertTrue(e2eRecommendationsPage.setOptioninPrompt(prompt, value));

    }

    @And("^In \"([^\"]*)\" type \"([^\"]*)\"$")
    public void typeValue(String prompt, String value){
        e2eRecommendationsPage.waitForPageLoad();
        Log.info("Prompt options: " + e2eRecommendationsPage.getPromptNames());
        assertTrue(e2eRecommendationsPage.typeOptioninPrompt(prompt, value), "The prompt is not available");
    }

    @And("^Click the \"([^\"]*)\" button$")
    public void clickButton(String buttonName) {
        assertTrue(e2eRecommendationsPage.isButton(buttonName), "The " + buttonName + " button is not available");
        e2eRecommendationsPage.clickButton(buttonName);
    }

    @And("^Is redirected to \"([^\"]*)\" page$")
    public void redirectedUrl(String redirUrl) {
        e2eRecommendationsPage.waitForPageLoad();
        if(redirUrl.equals("360-oncology")){
            e2eRecommendationsPage.waitForElementVisible(e2eRecommendationsPage.varianLogo);
        }
        assertTrue(cdsHookAppPage.getCurrentUrl().contains(redirUrl), "Is not redirected to " + redirUrl + ", but to: " + cdsHookAppPage.getCurrentUrl());
    }

    @And("^The Recommendations page is shown")
    public void showRecommendations() {
        assertTrue((cdsHookAppPage.isRecommendationsPage()));
    }

    @And("^I set \"([^\"]*)\" recommendation as \"([^\"]*)\"$")
    public void setRecommendationAs(String treatment, String treatmentStatus) {
        assertTrue(e2eRecommendationsPage.isTreatment(treatment), "Treatment is not present");
        assertTrue(e2eRecommendationsPage.isStatus(treatment, treatmentStatus),"Status is not available");
        e2eRecommendationsPage.statusCompleted(treatment);
    }

    @And("^Check \"([^\"]*)\" is still \"([^\"]*)\"$")
    public void checkIsStill(String treatment, String treatmentStatus) {
        assertTrue(e2eRecommendationsPage.isTreatment(treatment), "Treatment is not present");
        assertTrue(e2eRecommendationsPage.isPersistentStatus(treatmentStatus),"Status is not available");
    }

    @Then("^I am redirected to 360-oncology")
    public void redirectedTo360() {
        assertTrue(e2eRecommendationsPage.currentUrlRedirect().equals(e2eRecommendationsPage.getRedirectFromConfig()));
    }

    @Then("^Get the Patient Record")
    public void getPatientRecord() throws IOException {
        patientRecord = cdsHookAppPage.getPatientRecord();
        Log.info("The patient record is: " + patientRecord);
    }

    @And ("^Get the Bundle and save it as \"([^\"]*)\"$")
    public void getBundle(String bundleName) throws IOException{
        String bundleData = cdsHookAppPage.getBundle();
        Log.info ("Bundle: " + bundleData);
        cdsHookAppPage.deleteBundleIfExists(bundleName);
        cdsHookAppPage.saveBundle(bundleName, bundleData);
    }

    @And("^Check \"([^\"]*)\" \"([^\"]*)\" value = \"([^\"]*)\"$")
    public void checkJsonValues(String parentKey, String guidelineResource, String value) {
        DocumentContext patientRecordJson = JsonPath.parse(patientRecord);
        if (parentKey.equals("Observation")) {
            if (guidelineResource.equals("Tumor Size")){
                JsonPath jsonPath = JsonPath.compile(jsonPathTumorSize);
                valueFromJson= patientRecordJson.read(jsonPath);
            }
            if (guidelineResource.equals("Estrogen Receptor Status")){
                JsonPath jsonPath = JsonPath.compile(jsonPathEstrogenReceptor);
                valueFromJson= patientRecordJson.read(jsonPath);
            }
            if (guidelineResource.equals("Context")){
                JsonPath jsonPath = JsonPath.compile(jsonPathContextReference);
                valueFromJson= patientRecordJson.read(jsonPath);
            }
        }
        if (parentKey.equals("Procedure Request")){
            if (guidelineResource.equals("Simple mastectomy")){
                JsonPath jsonPath = JsonPath.compile(jsonPathSimpleMastectomy);
                valueFromJson= patientRecordJson.read(jsonPath);
            }
            if (guidelineResource.equals("Sentinel node biopsy")){
                JsonPath jsonPath = JsonPath.compile(jsonPathSentinelNodeBiopsy);
                valueFromJson= patientRecordJson.read(jsonPath);
            }
        }
        if (parentKey.equals("Document Reference")){
            if (guidelineResource.equals("Patient Path url")){
                JsonPath jsonPath = JsonPath.compile((jsonPathPatientPathUrl));
                valueFromJson = patientRecordJson.read(jsonPath);
            }
        }
        assertTrue(valueFromJson.get(0).toString().contains(value), "The expected value is " + value + " but we receive: " + valueFromJson);
    }

    @Given("^I open the Guidelines url$")
    public void openGuidelineLoginWebsite() {
        mainWindow = loginPage.openGuidelineUrl();
    }

    @And("^I move to the \"([^\"]*)\" tab")
    public void moveLoginTab(String tab) {
        cdsHookAppPage.changeTab(tab);
    }

    @And("^Check \"([^\"]*)\" \"([^\"]*)\" includes \"([^\"]*)\"$")
    public void checkJsonValuesInclude(String parentKey, String guidelineResource, String value) {
        DocumentContext patientRecordJson = JsonPath.parse(patientRecord);
        if (parentKey.equals("Document Reference")){
            if (guidelineResource.equals("Patient Path url")){
                JsonPath jsonPath = JsonPath.compile((jsonPathPatientPathUrl));
                valueFromJson = patientRecordJson.read(jsonPath);
            }
        }
        assertTrue(valueFromJson.get(0).toString().contains(value), "The expected value is " + value + " but we receive: " + valueFromJson);
    }

    @And("^Navigate to \"([^\"]*)\" and returns \"([^\"]*)\"$")
    public void navigateToUrl(String urlName, String urlResponse){
        String patientPathUrl = valueFromJson.get(0).toString();
        if (urlName.equals("Patient Path url")){
            cdsHookAppPage.navigateTo(patientPathUrl);
            cdsHookAppPage.waitForPageLoad();
            if (urlResponse.equals("Success")){
                String currentUrl = cdsHookAppPage.getCurrentUrl().toString();
                Log.info("Current url " + currentUrl + "\n");
                assertTrue(currentUrl.contains(patientPathUrl), "The expected value is " + patientPathUrl + " but we receive " + currentUrl);
            }
            if (urlResponse.equals("404")){
                String currentUrl = cdsHookAppPage.getCurrentUrl().toString();
                Log.info("Current url " + currentUrl + "\n");
                assertTrue(currentUrl.contains(urlResponse), "The expected value is 404 Guideline not available but we receive " + currentUrl);
            }
        }

    }

    @And("^Set \"([^\"]*)\" in line \"([^\"]*)\"$")
    public void setOption(String optionOne, Integer lineNumber){
        Integer elementNumber = lineNumber - 1;
        e2eRecommendationsPage.clickInputLine(elementNumber.toString());
        e2eRecommendationsPage.clickSelection(optionOne);
    }

    @And ("^Select \"([^\"]*)\" checkbox$")
    public void setCheckBox(String checkBoxName){
        assertTrue(e2eRecommendationsPage.isCheckBox(checkBoxName), "The " + checkBoxName + " checkbox is not available");
        e2eRecommendationsPage.clickSelection(checkBoxName);
    }
}
