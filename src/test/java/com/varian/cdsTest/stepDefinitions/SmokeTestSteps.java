package com.varian.cdsTest.stepDefinitions;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import com.varian.autotest.cds.cucumber.TestContext;
import com.varian.autotest.cds.logging.Log;
import com.varian.autotest.cds.pageObjects.CDSHookAppPage;
import com.varian.autotest.cds.pageObjects.GuidelinePage;
import com.varian.autotest.cds.pageObjects.LoginPage;
import com.varian.autotest.cds.pageObjects.E2eRecommendationsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SmokeTestSteps {

    private TestContext testContext;
    private LoginPage loginPage;
    private GuidelinePage guidelinePage;
    private E2eRecommendationsPage e2eRecommendationsPage;
    private CDSHookAppPage cdsHookAppPage;

    public SmokeTestSteps(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
        guidelinePage = testContext.getPageObjectManager().getGuidelinePage();
        e2eRecommendationsPage = testContext.getPageObjectManager().getE2eRecommendationsPage();
        cdsHookAppPage = testContext.getPageObjectManager().getCDSHookAppPage();
    }

    @Given("^Patient Record has \"([^\"]*)\"$")
    public void PatientRecordHas(String dataReq1) throws IOException {
        Log.info("Retrieve FHIR Resources " + dataReq1 + " add to JSON Payload");
        String inputJson= cdsHookAppPage.getUpdatedPatientJsonDataMultiple(dataReq1);
        Log.info(inputJson);

    }

    @Then("^\"([^\"]*)\" is \"([^\"]*)\"$")
    public void treatmentIs(String treatment, String treatmentStatus){
        if (treatmentStatus.equals("Completed")){
            assertTrue(cdsHookAppPage.isCompleted(),treatment + " is not set as Completed");
        }
        if(treatmentStatus.equals("Active")) {
            assertTrue((cdsHookAppPage.isActive()), treatment + " is not set as Active");
        }
    }

    @And("^Add GuidanceResponse$")
    public void addGuidanceResponse() throws IOException{
        cdsHookAppPage.addGuidanceResponse();
        Log.info("Guidance Response added");
    }

}
