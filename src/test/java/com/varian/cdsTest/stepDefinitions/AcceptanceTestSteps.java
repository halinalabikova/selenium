package com.varian.cdsTest.stepDefinitions;

import com.varian.autotest.cds.cucumber.TestContext;
import com.varian.autotest.cds.logging.Log;
import com.varian.autotest.cds.pageObjects.CDSHookAppPage;
import com.varian.autotest.cds.pageObjects.E2eRecommendationsPage;
import com.varian.autotest.cds.pageObjects.GuidelinePage;
import com.varian.autotest.cds.pageObjects.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class AcceptanceTestSteps {
    private TestContext testContext;
    private LoginPage loginPage;
    private GuidelinePage guidelinePage;
    private CDSHookAppPage cdsHookAppPage;
    private E2eRecommendationsPage e2eRecommendationsPage;


    private String guidelineJsonFileName;
    public String cdsHookAppUrl;

    public AcceptanceTestSteps(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
        guidelinePage = testContext.getPageObjectManager().getGuidelinePage();
        cdsHookAppPage = testContext.getPageObjectManager().getCDSHookAppPage();
        e2eRecommendationsPage = testContext.getPageObjectManager().getE2eRecommendationsPage();
    }

    @When("^Check the guideline \"([^\"]*)\" state$")
    public void checkGuidelineUploaded(String fileName) {
        if (guidelinePage.verifyGuidelineIsCreated(fileName)){
            Log.info("Guideline " + fileName + " is loaded");
        }
        else {
            Log.info("Loading guideline " + fileName);
            uploadGuidelineFile(fileName);
        }
    }

    @When("^Upload \"([^\"]*)\" guideline file$")
    public void uploadGuidelineFile(String fileName) {
        Log.info("Uploading the guideline");
        assertTrue(guidelinePage.isGuidelineUploadButtonDisplay(), "The Upload guideline button is not available");
        guidelinePage.onUploadAGuidelineJsonFile(fileName);
        guidelinePage.waitForGuidelineUploaded();
        Log.info("And new guideline is created");
        assertTrue(guidelinePage.verifyGuidelineIsCreated(fileName),"The Guideline " + fileName + " was not created");
    }

    @Then("^Change the guideline \"([^\"]*)\" status Under Test$")
    public void setGuidelineStatusToUnderTest(String jsonfileName) {
        Log.info("Check if guideline is Under Test");
        if (!guidelinePage.isGuidelineStatusIsUnderTest(jsonfileName)){
            Log.info("Then Set the guideline status Under Test");
            boolean underTestClicked = guidelinePage.setGuidelineStatusToUnderTest(jsonfileName);
            assertTrue(underTestClicked, "The Under test button was not available");
        }
    }

    @Then("^Change the guideline \"([^\"]*)\" status Draft$")
    public void setGuidelineStatusToDraft(String jsonfileName) {
        assertTrue(guidelinePage.isGuidelineUploadButtonDisplay(), "Guideline page is not loaded");
        Log.info("Then Set the guideline status Draft");
        boolean underTestClicked = guidelinePage.setGuidelineStatusToDraft(jsonfileName);
        assertTrue(underTestClicked, "The Draft button was not available");
        assertTrue(guidelinePage.isGuidelineStatusIsDraft(jsonfileName), "The Guideline was not set to Draft");
    }

    @Given("^Empty Patient Record in JSON Payloads$")
    public void emptyPatientRecordInJsonPayload() throws IOException {
        Log.info("Empty Patient Record in JSON Payloads.");
        String inputJson = cdsHookAppPage.getUpdatedPatientJsonData();
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasFhirData1(String fHIRData1) throws IOException {
        Log.info("Retrieve FHIR Resources " + fHIRData1 + " add to JSON Payload");
        String inputJson = cdsHookAppPage.getUpdatedPatientJsonData(fHIRData1);
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\" and \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasFhirData1AndFhirData2(String fHIRData1, String fHIRData2) {
        Log.info("Retrieve FHIR Resources " + fHIRData1 + " " + fHIRData2 + "add to JSON Payload");
        String inputJson = cdsHookAppPage.getUpdatedPatientJsonData(fHIRData1, fHIRData2);
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasFhirData1AndFhirData2AndFhirData3(String fHIRData1, String fHIRData2, String fHIRData3) {
        Log.info("Retrieve FHIR Resources " + fHIRData1 + " " + fHIRData2 + "" + fHIRData3 + "add to JSON Payload");
        String inputJson = cdsHookAppPage.getUpdatedPatientJsonData(fHIRData1, fHIRData2, fHIRData3);
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\" \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasFhirData1WithDiffStatus(String status, String fHIRData1) {
        Log.info("Retrieve FHIR Resources " + status + " " + fHIRData1 + "add to JSON Payload");
        String inputJson = cdsHookAppPage.getModifiedPatientJsonData(status, fHIRData1);
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasFhirData1WithDiffStatusAndFhirData1(String status, String fHIRData1, String fHIRData2) {
        Log.info("Retrieve FHIR Resources " + fHIRData1 + "" + status + "" + fHIRData2 + "add to JSON Payload");
        String inputJson = cdsHookAppPage.getModifiedPatientJsonData(status, fHIRData1, fHIRData2);
        Log.info(inputJson);
    }

    @Given("^Patient Record has \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\", add to JSON Payloads$")
    public void patientRecordHasDataReq1WithDiffStatusAndDataReq2AndDateReq3(String status, String fHIRData1, String fHIRData2, String fHIRData3) {
        Log.info("Retrieve FHIR Resources " + fHIRData1 + "" + status + "" + fHIRData2 + "add to JSON Payload");
        String inputJson = cdsHookAppPage.getModifiedPatientJsonData(status, fHIRData1, fHIRData2, fHIRData3);
        Log.info(inputJson);
    }

    @Given("^Patient Record no Resources and has GuidanceResponse$")
    public void emptyPatientRecordWithGuidanceResponse() throws IOException {
        Log.info("Empty Patient Record With GuidanceResponse in JSON Payloads.");
        String inputJson = cdsHookAppPage.getUpdatedPatientJsonDataWithGuidanceResponse();
        Log.info(inputJson);
    }

    @Given("^Add \"([^\"]*)\"$")
    public void addBundle(String bundle) throws IOException {
        Log.info("Add Bundle to json request");
        String inputJson = cdsHookAppPage.addBundle(bundle);
        Log.info(inputJson);
    }

    @Given("^Add temp Bundle \"([^\"]*)\"$")
    public void addTempBundle(String bundle) throws IOException {
        Log.info("Add Bundle to json request");
        String inputJson = cdsHookAppPage.addTempBundle(bundle);
        Log.info(inputJson);
    }

    @And("^Send POST API Request to CDS app services$")
    public void sendPOSTRequest() throws IOException {
        cdsHookAppUrl = cdsHookAppPage.sendPostToCdsHookApp();
    }

    @Then("^Guideline is not triggered$")
    public void guidelineIsNotTriggered() {
        Log.info("Guideline is not triggered");
        cdsHookAppPage.waitForPageLoad();
        assertEquals("There is currently no published guideline for this patient's condition.", cdsHookAppPage.GuidelineIsNotTriggeredMessage());
    }

    @Then("^Guideline is triggered,Prompt for C50, ER, PR$")
    public void promptForC50ERPR() {
        Log.info("Guideline is triggered,Prompt for C50, ER, PR");
        guidelinePage.waitForPageLoad();
        assertEquals("C50", cdsHookAppPage.getPromptResults().get(0));
        assertEquals("ER", cdsHookAppPage.getPromptResults().get(1));
        assertEquals("PR", cdsHookAppPage.getPromptResults().get(2));
    }

    @Then("^Guideline is triggered, Prompt for ER, PR$")
    public void promptForERPR() {
        Log.info("Guideline is triggered, Prompt for ER,PR");
        assertEquals("ER", cdsHookAppPage.getPromptResults().get(0));
        assertEquals("PR", cdsHookAppPage.getPromptResults().get(1));
    }

    @Then("^Guideline is triggered, Prompt for C50, PR$")
    public void promptForC50PR() {
        Log.info("Guideline is triggered, Prompt for C50,PR");
        assertEquals("C50", cdsHookAppPage.getPromptResults().get(0));
        assertEquals("PR", cdsHookAppPage.getPromptResults().get(1));
    }

    @Then("^Guideline is triggered, Prompt for C50$")
    public void promptForC50() {
        Log.info("Guideline is triggered, Prompt for C50");
        assertEquals("C50", cdsHookAppPage.getPromptResults().get(0));
    }

    @Then("^Guideline is triggered, Prompt for ER$")
    public void promptForER() {
        Log.info("Guideline is triggered, Prompt for ER");
        assertEquals("ER", cdsHookAppPage.getPromptResults().get(0));
    }

    @Then("^Guideline is triggered, Prompt for ER, cT$")
    public void promptForCTER() {
        Log.info("Guideline is triggered, Prompt for ER, cT");
        assertEquals("ER", cdsHookAppPage.getPromptResults().get(0));
        assertEquals("cT", cdsHookAppPage.getPromptResults().get(1));
    }

    @Then("^Guideline is triggered, Don't Prompt, Recommend Lumpectomy$")
    public void promptRecommendLumpectomy() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend Lumpectomy");
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("Lumpectomy", recommendations.get(0));
    }

    @Then("^Guideline is triggered, Prompt for cT$")
    public void promptForCt() {
        Log.info("Guideline is triggered, Prompt for cT");
        assertEquals("cT", cdsHookAppPage.recommendResults());
    }

    @Then("^Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy$")
    public void promptRecommendLumpectomyMastectomy() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend Lumpectomy, Mastectomy");
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("Lumpectomy", recommendations.get(0));
        assertEquals("Mastectomy", recommendations.get(1));
    }

    @Then("^Guideline is triggered,Recommend Lumpectomy and Mastectomy, Recommend ROB$")
    public void promptRecommendLumpectomyMastectomyROB() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend Lumpectomy and Mastectomy, Recommend ROB");
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("Lumpectomy", recommendations.get(0));
        assertEquals("Mastectomy", recommendations.get(1));
        assertEquals("ROB", recommendations.get(2));
    }

    @Then("^Guideline is triggered with GuidanceResponse, Recommend ROB$")
    public void withGuidanceResponsePromptRecommendROB() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend ROB");
        cdsHookAppPage.clickNextButton();
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("ROB", recommendations.get(0));
    }

    @Then("^Guideline is triggered, Don't Prompt, Recommend ROB$")
    public void promptRecommendROB() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend ROB");
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("ROB", recommendations.get(0));
    }

    @Then("^Guideline is triggered, Don't Prompt, Recommend Completed Lumpectomy, Active Mastectomy$")
    public void promptRecommendCompletedLumpectomyActiveMastectomy() {
        Log.info("Guideline is triggered, Don't Prompt, Recommend Completed Lumpectomy, Active Mastectomy");
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals("Lumpectomy", recommendations.get(0));
        assertEquals("Mastectomy", recommendations.get(1));
        List<String> allStatus = cdsHookAppPage.getAllLockedStatus();
        assertEquals(2, allStatus.size());
        assertEquals("completed", allStatus.get(0));
        assertEquals("active", allStatus.get(1));
    }

    @Then("^Off guideline message$")
    public void offGuidelineMessage() {
        Log.info("Off guideline message");
        cdsHookAppPage.waitForPageLoad();
        assertEquals("The patient's care is outside of the guideline recommended treatments so no recommendation can be provided.", cdsHookAppPage.offGuildelineMessage());
    }

    @Then("^No additional Recommendations$")
    public void noAdditionalRecommendationse() {
        Log.info("No additional Recommendations");
        cdsHookAppPage.waitForPageLoad();
        assertEquals("There are no additional recommendations for the patient at this time.", cdsHookAppPage.noAdditionRecMessage());
    }

    @When("^Launch the CDS Hook App$")
    public void launchCdsHookAppUrl() {
        Log.info("The url from previous step is: " + cdsHookAppUrl);
        cdsHookAppPage.openPromptsPageURL(cdsHookAppUrl);
    }

    @Then("^Validate if all required recommendations display$")
    public void validateIfAllRequiredRecommendationsDisplay() {
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals(2, recommendations.size());
        assertEquals("Lumpectomy", recommendations.get(0));
        assertEquals("Mastectomy", recommendations.get(1));
    }

    @Then("^Validate if the required recommendations display$")
    public void validateIfRequiredRecommendationsDisplay() {
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals(1, recommendations.size());
        assertEquals("Lumpectomy", recommendations.get(0));
    }

    @Then("^Set Lumpectomy to completed status$")
    public void setLumpectomyToCompletedStatus() {
        cdsHookAppPage.setCompleted(0);
        List<String> allStatus = cdsHookAppPage.getAllActiveStatus();
        assertEquals("completed", allStatus.get(0));
    }

    @Then("^Guideline is triggered, Recommend ROB$")
    public void recommendROB() {
        guidelinePage.waitForPageLoad();
        Log.info("Guideline is triggeredRecommend ROB");
        assertTrue((cdsHookAppPage.isRecommendationsPage()));
        List<String> recommendations = cdsHookAppPage.getRecommendations();
        assertEquals(1, recommendations.size());
        assertEquals("ROB", recommendations.get(0));
    }

    //SmokeTest Steps
    @Then("^\"([^\"]*)\" is Recommended$")
    public void guidanceIsRecommended (String guidance) {
        assertTrue(e2eRecommendationsPage.isGuidance(guidance), "The expected Guidance: " + guidance + " is not present");
    }

    @Then("^\"([^\"]*)\" is Prompted$")
    public void guidanceIsPrompted (String guidance) {
        assertTrue(e2eRecommendationsPage.isGuidance(guidance), "The expected Guidance: " + guidance + " is not present");
    }

    @And("^Send POST API Request to CDS app$")
    public void sendPOSTRequestSmoke() throws IOException {
        cdsHookAppUrl = cdsHookAppPage.sendPostToCdsHookApp();
        cdsHookAppPage.openPromptsPageURL(cdsHookAppUrl);
    }

    @Then("^\"([^\"]*)\" is Recommended \"([^\"]*)\" times$")
    public void guidanceIsRecommended (String guidance, int times) {
        assertTrue(e2eRecommendationsPage.isGuidanceMultiple(guidance, times), "The expected Guidance: " + guidance + " is not present");
    }

    @Then("^There are \"([^\"]*)\"$")
    public void noRecommendations(String noRecommendations) {
        assertTrue(e2eRecommendationsPage.isGuidance(noRecommendations), "The expected message: " + noRecommendations + " is not present");
    }

    @And("^Delete the Guideline")
    public void deleteGuideline (){
        loginPage.openGuidelineListURL();
        if (guidelinePage.verifyGuidelineIsCreated(guidelinePage.guidelineName)) {
            Boolean guidelineDeleted = guidelinePage.deleteUploadGuideline(guidelinePage.guidelineName);
            assertTrue(guidelineDeleted, "The Guideline was not deleted");
        }
    }

    @When("^Upload \"([^\"]*)\" invalid guideline file$")
    public void uploadInvalidGuidelineFile(String fileName) {
        Log.info("Uploading the guideline");
        assertTrue(guidelinePage.isGuidelineUploadButtonDisplay(), "The Upload guideline button is not available");
        guidelinePage.onUploadInvalidGuidelineJsonFile(fileName);
    }

}
