package com.varian.autotest.cds;

import static com.varian.autotest.cds.managers.FileReaderManager.getConfigReader;

public class Index {

    public static final String GUIDELINE_LIST = "guidelines";
    public static final String SENDPOST_URL = "api/rest/cds-services/app";
    public static final String SENDPOST_URL_SUGGESTIONS = "api/rest/cds-services/suggestions";
    public static final String BUNDLE_URL = "api/rpc/bundle-generator/generate-complete-bundle?cdsHooksInstanceId=";

    public static String getPath(String path) {
        return getConfigReader().getDeploymentUrl() + path;
    }

    public static String getLoginName() {
        return getConfigReader().getUsername();
    }

    public static String getLoginPassword() {
        return getConfigReader().getPassword();
    }

    public static String getRedirectUrl() {return getConfigReader().getRedirectUrl(); }

}
