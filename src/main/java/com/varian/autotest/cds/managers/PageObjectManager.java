package com.varian.autotest.cds.managers;

import org.openqa.selenium.WebDriver;

import com.varian.autotest.cds.pageObjects.CDSHookAppPage;
import com.varian.autotest.cds.pageObjects.GuidelinePage;
import com.varian.autotest.cds.pageObjects.LoginPage;
import com.varian.autotest.cds.pageObjects.E2eRecommendationsPage;

public class PageObjectManager {

    private WebDriver driver;
    private LoginPage loginPage;
    private GuidelinePage guidelinePage;
    private E2eRecommendationsPage e2eRecommendationsPage;
    private CDSHookAppPage cdsHookAppPage;


    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage getLoginPage() {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }

    public GuidelinePage getGuidelinePage() {
        return (guidelinePage == null) ? guidelinePage = new GuidelinePage(driver) : guidelinePage;
    }

    public E2eRecommendationsPage getE2eRecommendationsPage() {
        return (e2eRecommendationsPage == null) ? e2eRecommendationsPage = new E2eRecommendationsPage(driver) : e2eRecommendationsPage;
    }

    public CDSHookAppPage getCDSHookAppPage() {
        return (cdsHookAppPage == null) ? cdsHookAppPage = new CDSHookAppPage(driver) : cdsHookAppPage;
    }

}