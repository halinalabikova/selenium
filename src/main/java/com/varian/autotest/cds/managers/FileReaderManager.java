package com.varian.autotest.cds.managers;

import com.varian.autotest.cds.dataProviders.ConfigFileReader;


public class FileReaderManager {

    private static ConfigFileReader configFileReader;


    private FileReaderManager() {
    }

    public static ConfigFileReader getConfigReader() {
        if (configFileReader == null) {
            configFileReader = new ConfigFileReader();
        }
        return configFileReader;
    }

}