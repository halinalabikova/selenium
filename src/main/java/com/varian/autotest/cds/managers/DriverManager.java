package com.varian.autotest.cds.managers;

import com.varian.autotest.cds.enums.DriverType;
import com.varian.autotest.cds.enums.EnvironmentType;
import com.varian.autotest.cds.logging.Log;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.varian.autotest.cds.managers.FileReaderManager.getConfigReader;

public class DriverManager {
    private WebDriver driver;
    private static DriverType driverType;
    private static EnvironmentType environmentType;

    public DriverManager() {
        driverType = getConfigReader().getBrowser();
        environmentType = getConfigReader().getEnvironment();
    }

    public WebDriver getDriver() {
        if (driver == null) {
            driver = createDriver();
        }
        return driver;
    }

    private WebDriver createDriver() {
        switch (environmentType) {
            case LOCAL:
                driver = createLocalDriver();
                break;
        }
        return driver;
    }

    private WebDriver createLocalDriver() {
        switch (driverType) {
            case CHROME:
                WebDriverManager.chromedriver().version(getConfigReader().getWebdriverversion());
                WebDriverManager.chromedriver().setup();
                //System.setProperty("webdriver.chrome.verboseLogging", "true"); //Webdriver logs
                driver = new ChromeDriver(getChromeOptions());
                break;

            case INTERNETEXPLORER:
                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
        }

        if (getConfigReader().getBrowserWindowMaximize()) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
        return driver;
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        Log.info("Is headless: " + getConfigReader().isHeadless());
        if (getConfigReader().isHeadless()) {
           chromeOptions.addArguments("--headless");
        }
        chromeOptions.addArguments("--kiosk");
        return chromeOptions;
    }

    public void closeDriver() throws IOException {
        Log.info("Closing the selenium session");
        String currentOs = System.getProperty("os.name");
        Log.info ("The os used is: " + currentOs);
        //There is an issue in the pipeline where we cannot use driver.quit() and
        // the browser and driver are not closed if driver.close() is used so we need to terminate them manually
        if (currentOs.equals("Linux")) {
            driver.close();
            Runtime rt = Runtime.getRuntime();
            rt.exec("pkill chrom");
        }
        else {
            driver.quit();
        }
    }
}