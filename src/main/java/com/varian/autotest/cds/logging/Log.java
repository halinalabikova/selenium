package com.varian.autotest.cds.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {

// Initialize Log4j logs

    private static Logger log = LogManager.getLogger(Log.class);


    private Log() {

    }

    // This is to print log for the beginning of the test case, as we usually run so many test cases as a test suite

    public static void startTestCase(String sTestCaseName) {

        info("****************************************************************************************");

        info("****************************************************************************************");

        info("$$$$$$$$$$$$$$$$$$$$$                 " + sTestCaseName + "       $$$$$$$$$$$$$$$$$$$$$$$$$");

        info("****************************************************************************************");

        info("****************************************************************************************");
    }

    //This is to print log for the ending of the test case

    public static void endTestCase(String sTestCaseName) {

        info("XXXXXXXXXXXXXXXXXXXXXXX             " + "-E---N---D-" + "             XXXXXXXXXXXXXXXXXXXXXX");

        info("X");

        info("X");

        info("X");

        info("X");
    }

    // Need to create these methods, so that they can be called

    public static void info(String message) {

        log.info(message);
    }

    public static void warn(String message) {

        log.warn(message);
    }

    public static void error(String message, Throwable t) {

        log.error(message, t);
    }

    public static void fatal(String message) {

        log.fatal(message);
    }

    public static void debug(String message) {

        log.debug(message);
    }

    //LogData assertion result
    public static void log(String command, String description, boolean success) {
        log.info(command, description, success, false);
    }

    public static void log(String command, Throwable e, boolean success) {
        log.info(command, e.getMessage(), success, false);
    }

    public static void log(
            String command, String descriptionOnSuccess, String descriptionOnFail, boolean success
    ) {
        String description = descriptionOnFail;
        if (success) {
            description = descriptionOnSuccess;
        }
        log.info(command, description, success, false);
    }
}
