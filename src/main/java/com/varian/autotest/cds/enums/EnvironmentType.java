package com.varian.autotest.cds.enums;

public enum EnvironmentType {
    LOCAL,
    REMOTE,
}
