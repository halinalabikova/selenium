package com.varian.autotest.cds.enums;

public enum DriverType {
    FIREFOX,
    CHROME,
    INTERNETEXPLORER
}