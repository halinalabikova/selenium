package com.varian.autotest.cds.pageObjects;

import static com.varian.autotest.cds.Index.*;
import static com.varian.autotest.cds.utils.TestJsonData.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.varian.autotest.cds.logging.Log;

public class CDSHookAppPage extends BasePageObject {

    public static final String COOKIE_FILE = "Cookies.data";

    @FindBy(xpath = "//h5[contains(text(),'There is currently no published guideline for this')]")
    public WebElement textContent;

    @FindBy(xpath = "//h5[contains(text(),\"The patient's care is outside of the guideline rec\")]")
    public WebElement offGuideline;

    @FindBy(xpath = "//h5[contains(text(),'There are no additional recommendations for the pa')]")
    public WebElement noAdditionRec;

    @FindBy(xpath = "//span[@mattooltipposition='right']")
    public List<WebElement> recommendCheckboxes;

    @FindBy(xpath = "//span[@class='mat-checkbox-label']")
    public List<WebElement> recommendTexts;

    //Added to find titles in Acceptance
    @FindBy(xpath = "//*[@class='title']")
    public List<WebElement> recommemdCheckboxesTitles;

    @FindBy(xpath = "//div[@class='body']//div[@class='prompt-checkbox']")
    public List<WebElement> promptCheckboxes;

    @FindBy(xpath = "//*[@class='fa fa-check-circle selected-locked']")
    public WebElement recommendationSetAsCompleted;

    @FindBy(xpath = "//*[@class='fa fa-clock-o selected-locked']")
    public WebElement recommendationSetAsActive;

    @FindBy(xpath = "//span[@mattooltipposition='right']")
    public List<WebElement> recommemdCheckboxes;

    @FindBy(xpath = "//span[@class='mat-checkbox-label']")
    public List<WebElement> recommemdTexts;

    @FindBy(xpath = "//span[contains(text(),'Recommendations')]")
    public WebElement recommendationsTitle;

    @FindBy(xpath = "//*[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
    public WebElement recommendationBox;

    @FindBy(xpath = "//*[@class='fa fa-check-circle']")
    public WebElement recommendationActive;

    @FindBy(xpath = "//*[@class='fa fa-check-circle']")
    public List<WebElement> recommendationCompleted;

    @FindBy(xpath = "//button[@id='recommendations-next']")
    public WebElement nextRecommendationsButton;

    @FindBy(xpath = "//*[contains(text(),'Login')]/parent::button")
    public WebElement modalLoginButton;

    @FindBy(xpath = "//*[contains(text(),'Close')]/parent::button")
    public WebElement modalCloseButton;

    @FindBy(xpath = "//button[@id='back-btn']")
    public WebElement backRecommendationsButton;

    @FindBy(xpath = "//button[@id='summary-copy-accepted']")
    public WebElement copyAcceptedButton;

    @FindBy(xpath = "//button[@id='default-done']")
    public WebElement doneButton;

    @FindBy(xpath = "//*[text()='Radiotherapy to Tumor bed']")
    public WebElement radiotherapyToTumor;

    @FindBy(xpath = "//*[text()='Aromatase Inhibitor']")
    public WebElement aromataseInhibitor;

    @FindBy(xpath = "//*[contains(@class, 'selected-active')]")
    public List<WebElement> allActiveStatus;

    @FindBy(xpath = "//*[contains(@class, 'selected-locked')]")
    public List<WebElement> allLockedStatus;

    private static final String inputFHIRDataJsonFolderPath = "payloads/fhirData/";
    private static final String inputFHIRDataTempBundlePath = "payloads/tempBundle/";
    private static final String inputPatientJsonFolderPath = "payloads/";
    private static final String inputTempFHIRDataJsonFolderPath = "src/test/resources/payloads/tempBundle/";

    private String inputJson;
    private String hookInstanceId;
    private String jsonResource = "{\"fullUrl\": \"http://fhir.evinance.fake/baseDstu3/GuidanceResponse/1\", \"search\": {\"mode\": \"match\"}, \"resource\": {}}";

    public List<String> tabs = new ArrayList<>();


    public CDSHookAppPage(WebDriver driver) {
        super(driver);
    }

    public String getOriginalPatientJsonData(String jsonFile) throws ParseException, IOException {
        inputJson = getFileContent(inputPatientJsonFolderPath + jsonFile);
        Log.info("Complete path: " + inputPatientJsonFolderPath + jsonFile);
        return inputJson;
    }

    //E2E
    public String getModifiedHookInstanceValueInJson(String jsonFile) throws ParseException, IOException {
        inputJson = getFileContent(inputPatientJsonFolderPath + jsonFile);
        hookInstanceId = generateRandomNDigitString(10);
        Log.info("HookInstanceId = " + hookInstanceId);
        inputJson = modifyValueInJson("hookInstance", hookInstanceId, inputJson);
        Log.info("Modified hookInstance in Input.json:");
        return inputJson;
    }

    public String getUpdatedPatientJsonDataWithGuidanceResponse() throws ParseException, IOException {
        inputJson = getFileContent(inputPatientJsonFolderPath + "Empty-Patient-Record-but-has-guidanceResponse.json");
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Empty Patient Record with guidance response in Input.json:");
        return inputJson;
    }

    public String addBundle(String bundle_name) throws ParseException, IOException {
        inputJson = getFileContent(inputFHIRDataJsonFolderPath + bundle_name +".json");
        inputJson = modifyValueInJson("hookInstance", hookInstanceId, inputJson);
        Log.info("Bundle " + bundle_name + " added" );
        return inputJson;
    }

    public String addTempBundle(String bundle_name) throws ParseException, IOException {
        inputJson = getFileContent(inputFHIRDataTempBundlePath + bundle_name +".json");
        inputJson = modifyValueInJson("hookInstance", hookInstanceId, inputJson);
        Log.info("Bundle " + bundle_name + " added" );
        return inputJson;
    }

//  Add original FHIR Data to the base input json

    public String getUpdatedPatientJsonData() throws ParseException,IOException{
        inputJson = getFileContent(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        hookInstanceId = generateRandomNDigitString(10);
        Log.info("HookInstanceId = " + hookInstanceId);
        inputJson = modifyValueInJson("hookInstance", hookInstanceId, inputJson);
        Log.info("Empty Patient Record in Input.json:");
        return inputJson;
    }

    public String getUpdatedPatientJsonData(String jsonFile) throws ParseException,IOException{
        String requiredFhirData = getFileContent(inputFHIRDataJsonFolderPath + jsonFile);
        inputJson = getFileContent(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = addValueInJson("entry", requiredFhirData, inputJson);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Modified suggested FHIR data in Input.json:");
        return inputJson;
    }

    public String getUpdatedPatientJsonData(String jsonFile1, String jsonFile2){
        String joined = getResourcesAsJsonArray(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile1,
                inputFHIRDataJsonFolderPath + jsonFile2
        ));
        inputJson = getResource(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = setJsonValue("entry", joined, inputJson);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        return inputJson;
    }

    public String getUpdatedPatientJsonData(String jsonFile1, String jsonFile2, String jsonFile3){
        String joined = getResourcesAsJsonArray(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile1,
                inputFHIRDataJsonFolderPath + jsonFile2,
                inputFHIRDataJsonFolderPath + jsonFile3
        ));
        inputJson = getResource(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = setJsonValue("entry", joined, inputJson);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        return inputJson;
    }
    public String getUpdatedPatientJsonDataMultiple(String jsonFile) throws ParseException, IOException {
        String requiredFhirData = getResource(inputFHIRDataJsonFolderPath + jsonFile);
        inputJson = addValueInJson("entry", requiredFhirData, inputJson);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Modified suggested FHIR data in Input.json:");
        return inputJson;
    }

    public String getUpdatedPatientJsonDataGuidance(String guidanceResponse) throws ParseException, IOException {
        //inputJson = jsonPayload;
        jsonResource = setJsonObjectValue("resource", guidanceResponse, jsonResource);
        inputJson = addValueInJson("entry", jsonResource, inputJson);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Modified suggested FHIR data in Input.json:");
        return inputJson;
    }

    //  Modify original FHIR Data status and add them to the base input json

    public String getModifiedPatientJsonData(String status, String jsonFile1){
        String joined = getResourcesAsJsonArray(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile1
        ));
        String updatedFHIRData = modifyValueInJson("status", status, joined);
        inputJson = getResource(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = setJsonValue("entry", updatedFHIRData, inputJson);
        Log.info("Modified recommendation status in json:" + updatedFHIRData);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Updated input:" + inputJson);
        return inputJson;
    }

    public String getModifiedPatientJsonData(String status, String jsonFile1, String jsonFile2){
        String fHIRData1 = getResourcesAsJsonArrayData(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile1
        ));
        String updatedFHIRData1 = modifyValueInJson("status", status, fHIRData1);
        String fHIRData2 = getResourcesAsJsonArrayData(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile2
        ));
        String updatedJoined = "[" + updatedFHIRData1 + "," + fHIRData2 + "]";
        inputJson = getResource(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = setJsonValue("entry", updatedJoined, inputJson);
        Log.info("Modified recommendation status in json:" + updatedJoined);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Updated input:" + inputJson);
        return inputJson;
    }

    public String getModifiedPatientJsonData(String status, String jsonFile1, String jsonFile2, String jsonFile3){
        String fHIRData1 = getResourcesAsJsonArrayData(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile1
        ));
        String updatedFHIRData1 = modifyValueInJson("status", status, fHIRData1);
        String fHIRData2 = getResourcesAsJsonArrayData(Arrays.asList(
                inputFHIRDataJsonFolderPath + jsonFile2,
                inputFHIRDataJsonFolderPath + jsonFile3
        ));
        String updatedJoined = "[" + updatedFHIRData1 + "," + fHIRData2 + "]";
        inputJson = getResource(inputPatientJsonFolderPath + "Empty-Patient-Record.json");
        inputJson = setJsonValue("entry", updatedJoined, inputJson);
        Log.info("Modified recommendation status in json:" + updatedJoined);
        inputJson = modifyValueInJson("hookInstance", generateRandomNDigitString(10), inputJson);
        Log.info("Updated input:" + inputJson);
        return inputJson;
    }

    // Send Post API to CDS Hook App

    public String sendPostToCdsHookApp() throws ParseException, IOException {
        String post_url = getSendPostURL();
        String cookie = getCookie();
        Log.info("The post message: " + inputJson);
        HttpResponse response = SendPostRequest(post_url, inputJson, cookie);
        String outStr = EntityUtils.toString(response.getEntity(), "UTF-8");
        Log.info(outStr);
        String url = fetchValueInJson("url", outStr);
        Log.info("The new url is: " + url);
        return url;
    }

    public String getCookie() {
        File file = new File(COOKIE_FILE);
        String cookie = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String strLine; (strLine = br.readLine()) != null; ) {
                Cookie ck = cookieFromString(strLine);
                if (ck != null) {
                    cookie = ck + ";" + cookie;
                }
            }
        }

        catch (Exception x) {
            Log.error("", x);
        }
        return cookie;
    }

    Cookie cookieFromString(String str) {
        if (str == null) {
            return null;
        }
        String[] tokens = str.split(";");
        if (tokens.length < 6) {
            return null;
        }
        return new Cookie.Builder(tokens[0], tokens[1])
                .domain(tokens[2])
                .path(tokens[3])
                .expiresOn("null".equals(tokens[4]) ? null : new Date(tokens[4]))
                .isSecure(Boolean.parseBoolean(tokens[5]))
                .build();

    }

    public void openPromptsPageURL(String promptUrl) {
        Log.info("we are receiving the right url: " + promptUrl + "\n");
        navigateTo(promptUrl);
    }

    public String getSendPostURL() {
        return getPath(SENDPOST_URL);
    }

    public String getBundleURL() {
        return getPath(BUNDLE_URL);
    }

    public String GuidelineIsNotTriggeredMessage() {
        return textContent.getText();
    }

    public String offGuildelineMessage() {
        return offGuideline.getText();
    }

    public String noAdditionRecMessage() {
        return noAdditionRec.getText();
    }

    public String recommendResults() {
        String result = "";
        for (WebElement chk : recommendTexts) {
            result += chk.getText().trim();
        }
        return result;
    }

    public List<String> getRecommendations() {
        return recommendCheckboxes.stream()
                .map(chk -> chk.getText().trim())
                .collect(Collectors.toList());
    }

    public List<String> getPromptResults() {
        return promptCheckboxes.stream()
                .map(chk -> chk.getText().trim())
                .collect(Collectors.toList());
    }

    public List<String> getAllLockedStatus() {
        return getAllStatus(allLockedStatus);
    }

    public List<String> getAllActiveStatus() {
        return getAllStatus(allActiveStatus);
    }

    public List<String> getAllStatus(List<WebElement> elements) {
        return elements.stream()
                .map(s -> s.getAttribute("class"))
                .map(cls -> {
                    if (cls.contains("fa-plus-circle")) {
                        return "draft";
                    } else if (cls.contains("fa-check-circle")) {
                        return "completed";
                    } else if (cls.contains("fa-clock-o")) {
                        return "active";
                    } else if (cls.contains("fa-ban")) {
                        return "cancelled";
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    public void setCompleted(int index) {
        new Actions(driver)
                .moveToElement(recommendCheckboxes.get(index))
                .click(recommendCheckboxes.get(index))
                .build()
                .perform();
        if (isElementDisplayed(recommendationCompleted.get(index))) {
            recommendationCompleted.get(index).click();
        }
    }

    public void clickNextButton() {
        nextRecommendationsButton.click();
    }

    public Boolean isRecommendationsPage() {
        wait.waitForRecommendationsPage();
        if (isElementOnPage(recommendationsTitle)) {
            waitForPageLoad();
            return true;
        }
        Log.info("There is no Recommendations page");
        return false;
    }

    public Boolean isTreatment(String pageTreatment) {
        if (pageTreatment.equals("Radiotherapy to Tumor bed")) {
            if (isElementOnPage(radiotherapyToTumor)) {
                return true;
            }
        }
        if (pageTreatment.equals("Aromatase Inhibitor")) {
            if (isElementOnPage(aromataseInhibitor)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isStatus(String pageStatus) {
        if (pageStatus.equals("Completed")) {
            if (isElementOnPage(recommendationActive)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isNextRecommendationsButton() {
        if (isElementOnPage(nextRecommendationsButton)) {
            return true;
        }
        return false;
    }

    public Boolean isCopyAcceptedButton() {
        waitForPageLoad();
        if (isElementOnPage(copyAcceptedButton)) {
            return true;
        }
        return false;
    }

    public Boolean isBackButton(){
        if (isElementOnPage(backRecommendationsButton)){
            return true;
        }
        return false;
    }

    public Boolean isDoneButton() {
        waitForPageLoad();
        if (isElementOnPage(doneButton)) {
            return true;
        }
        return false;
    }

    public Boolean isCompleted(){
        if(isElementOnPage(recommendationSetAsCompleted)) {
            return true;
        }
        return false;
    }

    public Boolean isActive(){
        if(isElementOnPage(recommendationSetAsActive)) {
            return true;
        }
        return false;
    }

    public Boolean isModalLogin(){
        waitForPageLoad();
        if(isElementOnPage(modalLoginButton)) {
            return true;
        }
        return false;
    }

    public Boolean isModalClose(){
        waitForPageLoad();
        if(isElementOnPage(modalCloseButton)) {
            return true;
        }
        return false;
    }

    public void clickButton(String button) {
        if (button.equals("Next")) {
            nextRecommendationsButton.click();
        }
        if (button.equals("Copy accepted")) {
            copyAcceptedButton.click();
        }
        pause(10000);
    }

    public void changeTab(String tab) {
        if (tab.equals("Login")) {
            for (String newTab:driver.getWindowHandles()){
                tabs.add(newTab);
            }
            driver.switchTo().window(tabs.get(1));
        Log.info("Switched to Login tab");
        }
        if (tab.equals("Guidelines")) {
            driver.switchTo().window(tabs.get(0));
            Log.info("Switched to Guidelines tab");
        }
    }

    public void statusActive() {
        recommendationBox.click();
        recommendationActive.click();
    }

    public String currentUrlRedirect() {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl;
    }

    public String getRedirectFromConfig() {
        String redirectFromConfig = getRedirectUrl();
        return redirectFromConfig;
    }

    public String getSendPostURLSuggestions() {
        return getPath(SENDPOST_URL_SUGGESTIONS);
    }

    public String addGuidanceResponse() throws ParseException, IOException{
        String guidanceResponse = sendPostSuggestions();
        inputJson = getUpdatedPatientJsonDataGuidance(guidanceResponse);
        Log.info(inputJson);
        return inputJson;
    }
    public String sendPostSuggestions() throws ParseException, IOException {
        String post_url = getSendPostURLSuggestions();
        String cookie = getCookie();
        Log.info("Cookie is: " + cookie);
        HttpResponse response = SendPostRequest(post_url, inputJson,cookie);
        String outStr = EntityUtils.toString(response.getEntity(), "UTF-8");
        String guidanceResponse = fetchValueInJsonArray("create", outStr);
        Log.info("GuidanceResponse: " + guidanceResponse);
        return guidanceResponse;
    }

    public String getPatientRecord() throws ParseException, IOException {
        waitForPageLoad();
        String post_url = getSendPostURL();
        String cookie = getCookie();
        Log.info("Cookie is: " + cookie);
        Log.info("Send post message to retrieve the patient record");
        Log.info("The post message: " + inputJson);
        HttpResponse response = SendPostRequest(post_url, inputJson, cookie);
        String outStr = EntityUtils.toString(response.getEntity(), "UTF-8");
        Log.info(outStr);
        return outStr;
    }

    public String getBundle() throws ParseException, IOException {
        String bundle_url = getBundleURL();
        String cookie = getCookie();
        Log.info("Send a request to get the Bundle");
        String getBundleUrl = bundle_url + hookInstanceId;
        HttpResponse response = SendGetRequest(getBundleUrl, cookie);
        String outStr = EntityUtils.toString(response.getEntity(),"UTF-8");
        Log.info(outStr);
        return outStr;
    }

    public void deleteBundleIfExists(String bundleFileName){
        Log.info("Path: " + inputTempFHIRDataJsonFolderPath + bundleFileName + ".json");
        File file = new File(inputTempFHIRDataJsonFolderPath + bundleFileName + ".json");
        // Delete old file if exists
        if(file.delete()){
            Log.info("Bundle file deleted");
        }
        else{
            Log.info("No Bundle file found");
        }
    }

    public void saveBundle(String bundleFileName, String bundleData) {
        Log.info("Path: " + inputTempFHIRDataJsonFolderPath + bundleFileName + ".json");
        File file = new File(inputTempFHIRDataJsonFolderPath + bundleFileName + ".json");
        try {
            file.createNewFile();
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                bw.write(bundleData.toString());
            }
        } catch(Exception x) {
            Log.error("", x);
        }
    }

    public String findPatientRecordValue (String patientRecord, String parentKey, String childKey){
        String valueFound = fetchValueInJson(patientRecord,parentKey,childKey);
        return valueFound;
    }
}
