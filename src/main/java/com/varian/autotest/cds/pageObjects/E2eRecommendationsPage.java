package com.varian.autotest.cds.pageObjects;

import static com.varian.autotest.cds.Index.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindBy;

import com.varian.autotest.cds.logging.Log;

public class E2eRecommendationsPage extends BasePageObject{

    @FindBy(xpath = "//*[text()='Prompts']")
    public WebElement promptsTitle;

    @FindBy(css = ".mat-form-field-infix")
    private List<WebElement> promptLine;

    @FindBy(xpath = "//*[@class='mat-checkbox-label' and contains(text(),'Estrogen Receptor Status')]//parent::*/child::div[@class='mat-checkbox-inner-container']")
    public WebElement estrogRecepStatusCheckbox;

    @FindBy(xpath = "//input[@id='mat-input-0']")
    public WebElement statusInput;

    @FindBy(xpath = "//*[contains(text(),'Positive')]")
    public WebElement statusPositive;

    @FindBy(xpath = "//*[contains(text(),'Negative')]")
    public WebElement statusNegative;

    @FindBy(xpath = "//*[contains(text(),'Status of estrogen')]")
    public WebElement statusOfEstrogen;

    @FindBy(xpath = "//*[contains(text(),'Estrogen Receptor Status')]")
    public WebElement estrogenReceptor;

    @FindBy(xpath = "//*[@class='mat-checkbox-label' and contains(text(),'Tumor size')]//parent::*/child::div[@class='mat-checkbox-inner-container']")
    public WebElement tumorSizeCheckbox;

    @FindBy(xpath = "//input[@id='mat-input-1']")
    public WebElement tumorSizeInput;

    @FindBy(xpath = "//input[@id='mat-input-2']")
    public WebElement tumorSizeInputAlone;

    @FindBy(xpath = "//button[@id='prompts-next']")
    public WebElement nextPromptButton;

    @FindBy(xpath = "//button[@id='back-btn']")
    public WebElement backButton;

    @FindBy(xpath = "//button[@id='default-done']")
    public WebElement doneButton;

    @FindBy(xpath = "//*[contains(text(),'Login')]/parent::button")
    public WebElement modalLoginButton;

    @FindBy(xpath = "//*[contains(text(),'Close')]/parent::button")
    public WebElement modalCloseButton;

    @FindBy(xpath = "//button[@id='recommendations-next']")
    public WebElement nextRecommendationsButton;

    @FindBy(css = ".mat-flat-button span.mat-button-wrapper")
    public  WebElement nextButton;

    @FindBy(xpath = "//button[@id='summary-copy-accepted']")
    public WebElement copyAcceptedButton;

    @FindBy(xpath = "//*[contains(text(),'biopsy')]/parent::*/parent::*/child::mat-checkbox")
    public WebElement recommendationCompletedActive;

    @FindBy(xpath = "//a[@class='logo']//img")
    public WebElement varianLogo;

    //@FindBy(xpath = "//*[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
    public WebElement recommendationBox;

    public WebElement recommendationCompleted;

    public E2eRecommendationsPage(WebDriver driver) {
        super(driver);
    }

    public void openPromptsPageURL(String promptUrl) {
        navigateTo(promptUrl);
    }

    public Boolean isPromptsPageOpen() {
        waitForPageLoad();
        if (isElementOnPage(promptsTitle)) {
            return true;
        }
        return false;
    }

    public Boolean isEstrogenReceptorStatusInput() {
        waitForPageLoad();
        wait.forElementPresent(By.xpath("//input[@id='mat-input-0']"), 5000);
        if (isElementOnPage(statusInput))  {
            return true;
        }
        return false;
    }

    public Boolean isTumorSizeCheckbox() {
        waitForPageLoad();
        wait.forElementPresent(By.xpath("//div[@class='mat-checkbox-inner-container']"), 5000);
        if (isElementOnPage(tumorSizeCheckbox)) {
            return true;
        }
        return false;
    }

    public Boolean isTumorSizeInput() {
        waitForPageLoad();
        wait.forElementPresent(By.xpath("//input[@id='mat-input-1']"));
        if (isElementOnPage(tumorSizeInput)) {
            return true;
        }
        return false;
    }

    public Boolean isOnlyTumorSizeInput() {
        waitForPageLoad();
        wait.forElementPresent(By.xpath("//input[@id='mat-input-2']"), 5000);
        if (isElementOnPage(tumorSizeInputAlone)) {
            return true;
        }
        return false;
    }

    public Boolean isNextPromptButton() {
        if (isElementOnPage(nextPromptButton)) {
            return true;
        }
        return false;
    }


    public void setPromptStatus(String promptStatus) {
       waitForPageLoad();
        estrogRecepStatusCheckbox.click();
        statusInput.click();
        if (promptStatus.equals("Positive")) {
            statusPositive.click();
        }
        if (promptStatus.equals("Negative")) {
            statusNegative.click();
        }
        if (promptStatus.equals("Status of estrogen receptors of neoplasm")){
            Log.info("Set Status of estrogen receptors of neoplasm");
            statusOfEstrogen.click();
        }
    }

    public void setMultiplePromptStatus(String promptStatus) {
       waitForPageLoad();
        estrogRecepStatusCheckbox.click();
        if (promptStatus.equals("Negative")) {
            tumorSizeInput.click();
            statusNegative.click();
        }
        if (promptStatus.equals("Positive")) {
            tumorSizeInput.click();
            statusPositive.click();
        }
        if (promptStatus.equals("Status of estrogen receptors of neoplasm")){
            statusInput.click();
            Log.info("Set Status of estrogen receptors of neoplasm");
            statusOfEstrogen.click();
        }
    }

    public void setPromptSize(int promptSize) {
        Log.info("The size is: " + promptSize);
        tumorSizeCheckbox.click();
        String stringSize = Integer.toString(promptSize);
        tumorSizeInput.sendKeys(stringSize);
    }

    public void setDataInput2(String dataInput) {
        Log.info("Set input: " + dataInput);
        tumorSizeInputAlone.click();
        tumorSizeInputAlone.sendKeys(dataInput);
    }

    public void setAlonePromptSize(int promptSize) {
        Log.info("The size is: " + promptSize);
        tumorSizeCheckbox.click();
        String stringSize = Integer.toString(promptSize);
        tumorSizeInputAlone.sendKeys(stringSize);
    }
    public void clickButton() {
        nextPromptButton.click();
    }

    public Boolean isGuidance(String guidance){
        if (isElementOnPage(driver.findElement(By.xpath("//*[contains(text(),'" + guidance + "')]")))) {
            Log.info("I found " + guidance + "\n");
            return true;
        }
        return false;
    }

    public Boolean isGuidanceMultiple(String guidance, int times) {
        List<WebElement> guidanceCount = driver.findElements(By.xpath("//*[contains(text(),'" + guidance + "')]/parent::div[@class='title']"));
        if (guidanceCount.size() == times) {
            return true;
        }
        return false;
    }

    public Boolean isButton(String button){
        if (button.equals("Next")) {
            waitForElementVisible(nextButton);
            if (isElementOnPage(nextButton)) {
                return true;
            }
        }
        if (button.equals("Copy accepted")){
            wait.forElementVisible(copyAcceptedButton, 5000);
            if (isElementOnPage(copyAcceptedButton)){
                return true;
            }
        }
        if (button.equals("Back")){
            if (isElementOnPage(backButton)){
                return true;
            };
        }
        if (button.equals("Done")){
            if (isElementOnPage(doneButton)){
                return true;
            }
        }
        return false;
    }

    public Boolean isCheckBox(String checkbox){
        waitForPageLoad();
        WebElement checkBoxName = driver.findElement(By.xpath("//*[contains(text(),'" + checkbox + "')]"));
        Log.info("Looking for " + checkbox);
        if (isElementOnPage(checkBoxName)) {
            return true;
        }
        return false;
    }

    public void clickSelection(String checkbox){
        WebElement checkBoxName = driver.findElement(By.xpath("//*[contains(text(),'" + checkbox + "')]/parent::*/parent::*/child::mat-checkbox"));
        checkBoxName.click();
    }

    public void clickInputLine(String inputLineNumber){
        WebElement inputLine = driver.findElement(By.id("mat-input-" + inputLineNumber));
        inputLine.click();
    }

    public void clickButton(String button) {
        if (button.equals("Next")) {
            nextButton.click();
        }
        if (button.equals("Copy accepted")) {
            copyAcceptedButton.click();
        }
        if (button.equals("Back")) {
            backButton.click();
        }
        if (button.equals("Done")) {
            doneButton.click();
        }
        Log.info("Click " + button + "\n");
    }

    public Boolean isTreatment(String pageTreatment) {
        waitForPageLoad();
        WebElement testTreatment = driver.findElement(By.xpath("//*[text()='" + pageTreatment + "']"));
        if (isElementOnPage(testTreatment)){
            return true;
        }
        return false;
    }

    public Boolean isStatus(String treatment, String pageStatus) {
        if (pageStatus.equals("Completed")) {
            recommendationCompleted = driver.findElement(By.xpath("//*[contains(text(),'" + treatment + "')]/parent::*/parent::*/child::*/i[@class='fa fa-check-circle']"));
            if (isElementOnPage(recommendationCompleted)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isPersistentStatus(String pageStatus) {
        if (pageStatus.equals("Completed")) {
            if (isElementOnPage(recommendationCompletedActive)) {
                return true;
            }
        }
        return false;
    }

    public void statusCompleted(String treatment) {
        recommendationBox = driver.findElement(By.xpath("//*[contains(text(),'" + treatment + "')]/parent::*/parent::*/child::mat-checkbox"));
        recommendationBox.click();
        recommendationCompleted.click();
    }

    public String currentUrlRedirect() {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl;
    }

    public String getRedirectFromConfig() {
        String redirectFromConfig = getRedirectUrl();
        return redirectFromConfig;
    }

    public List<String> getPromptNames(){
        return driver.findElements(By.cssSelector("mat-checkbox")).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public boolean setOptioninPrompt(String prompt, String value){
        int rowNumber = getPromptNames().indexOf(prompt);
        Log.info("The index is: " + rowNumber);
        WebElement promptLineActions = promptLine.get(rowNumber);
        new Actions(driver)
                .moveToElement(promptLineActions)
                .click(promptLineActions)
                .perform();
        Log.info("Click in the options line");
        driver.findElement(By.xpath("//*[contains(text(),'" + value + "')]")).click();
        Log.info(value + " is selected");
        return true;
    }

    public boolean typeOptioninPrompt(String prompt, String value){
        int rowNumber = getPromptNames().indexOf(prompt);
        Log.info("The index is: " + rowNumber);
        if (rowNumber >= 0){
            WebElement promptLineActions = promptLine.get(rowNumber);
            new Actions(driver)
                    .moveToElement(promptLineActions)
                    .click(promptLineActions)
                    .sendKeys(value)
                    .perform();
            Log.info("Click in the options line");
            Log.info(value + " is typed");
            pause(1000);
            return true;
        }
        return false;
    }
}
