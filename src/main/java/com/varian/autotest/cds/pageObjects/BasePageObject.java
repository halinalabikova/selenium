package com.varian.autotest.cds.pageObjects;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.varian.autotest.cds.selenium.Wait;
import com.varian.autotest.cds.logging.Log;

abstract class BasePageObject {

    protected WebDriver driver;
    protected Wait wait;
    private int timeOut = 15;
    public Actions builder;

    BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver);
        PageFactory.initElements(driver, this);
    }

    public void waitForPageLoad() {
        wait.waitForPageLoaded();
    }

    public void waitForElementVisible(WebElement element){
        wait.forElementVisible(element, 5000);
    }

    public void navigateTo(String url) {
        driver.navigate().to(url);
        wait.waitForPageLoaded();
    }

    public Set<String> getCurrentTabs() {
        return driver.getWindowHandles();
    }

    public void switchToFirstNewTab(Set<String> oldTabs) {
        Set<String> currentTabs = driver.getWindowHandles();
        String newTab = currentTabs.stream()
                .filter((tab) -> !oldTabs.contains(tab))
                .findFirst()
                .orElse(null);
        driver.switchTo().window(newTab);
    }

    public void switchTab() {
        String oldTab = driver.getWindowHandle();
        String newTab = driver.getWindowHandles()
                .stream()
                .filter((tab) -> !tab.equals(oldTab))
                .findFirst()
                .orElse(null);
        driver.switchTo().window(newTab);
    }

    public void openNewTab() {
        Set<String> oldTabs = driver.getWindowHandles();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.open()");
        String newTab = driver.getWindowHandles()
                .stream()
                .filter((tab) -> !oldTabs.contains(tab))
                .findFirst()
                .orElse(null);
        driver.switchTo().window(newTab);
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * Simple method for checking if element is on page or not.
     * Changing the implicitWait value allows
     * us no need for waiting 30 seconds
     */
    protected boolean isElementOnPage(By by) {
        changeImplicitWait(500, TimeUnit.MILLISECONDS);
        try {
            return driver.findElements(by).size() > 0;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    protected boolean isElementOnPage(WebElement element) {
        changeImplicitWait(1000, TimeUnit.MILLISECONDS);
        boolean isElementOnPage = true;
        try {
            // Get location on WebElement is rising exception when element is not present
            element.getLocation();
        } catch (WebDriverException ex) {
            isElementOnPage = false;
        } finally {
            restoreDefaultImplicitWait();
        }
        return isElementOnPage;
    }

    protected boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void refreshPage() {
        changeImplicitWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Log.log("refreshPage", "page refreshed", true);
    }

    public void changeImplicitWait(int value, TimeUnit timeUnit) {
        driver.manage().timeouts().implicitlyWait(value, timeUnit);
    }

    public void pause(Integer milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            Log.log("driver pause ", e, false);
        }
    }

    private void restoreDefaultImplicitWait() {
        changeImplicitWait(timeOut, TimeUnit.SECONDS);
    }

    /**
     * Method to click a element, when the element is not a button.
     */
    protected void clickElement(WebElement element) {
        builder.moveToElement(element).click().build().perform();
    }

    /**
     * Method to used to check unchecked element
     */
    protected void isChecked(WebElement element) {
        if(!element.isSelected()) element.click();
    }

    public void closeTheWindow() {
        driver.close();
    }
}