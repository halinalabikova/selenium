package com.varian.autotest.cds.pageObjects;

import com.varian.autotest.cds.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.varian.autotest.cds.selenium.Timeouts.*;
import static com.varian.autotest.cds.utils.TestJsonData.getAbsoluteFilePath;

public class GuidelinePage extends BasePageObject {

    @FindBy(css = "button .fa-upload")
    public WebElement uploadButton;
    @FindBy(xpath = "//input[@accept='.json']")
    public WebElement jsonInput;
    @FindBy(css = "button .fa-trash-o")
    public WebElement deleteButton;
    @FindBy(xpath = "//*[contains(@class,'no-decoration mat-raised-button mat-primary')]")
    public WebElement createButton;
    @FindBy(css = ".mat-table")
    public WebElement guidelineTable;

    //Action Submenu
    @FindBy(css = "div.mat-menu-content button:nth-child(1)")
    public WebElement testButton;
    @FindBy(xpath = "//div[@class='mat-menu-content']/button[2]")
    public WebElement approveButton;
    @FindBy(xpath = "//div[@class='mat-menu-content']/a[1]")
    public WebElement viewPathway;
    @FindBy(xpath = "//div[@class='mat-menu-content']/a[2]")
    public WebElement guidelineAdherence;
    @FindBy(xpath = "//div[@class='mat-menu-content']/a[3]")
    public WebElement createNewVersion;
    @FindBy(xpath = "//div[@class='mat-menu-content']/a[4]")
    public WebElement edit;
    @FindBy(xpath = "//div[@class='cdk-visually-hidden']")
    public WebElement notify;
    @FindBy(xpath = "//mat-table[@class='mat-table']//mat-row[9]//mat-cell[7]//button[1]")
    public WebElement buttonStatus;

    @FindBy(xpath = "//*[contains(text(),'You have inserted 1 guideline')]")
    public WebElement guidelineInserted;

    @FindBy(xpath = "//*[contains(text(),'Incompatible version')]")
    public WebElement guidelineRejected;

    @FindBy(xpath = "//*[contains(text(),'You have deleted 1 guideline')]")
    public WebElement guidelineDeleted;

    @FindBy(xpath = "//*[contains(text(),'Guideline is now under test')]")
    public WebElement guidelineUnderTest;

    @FindBy(xpath = "//*[contains(text(),'Guideline is now draft')]")
    public WebElement guidelineDraft;

    @FindBy(how = How.XPATH, xpath = "//mat-header-row/mat-header-cell")
    private List<WebElement> headers;
    @FindBy(how = How.XPATH, xpath = "//mat-checkbox[1]")
    private List<WebElement> checkboxs;
    @FindBy(how = How.XPATH, xpath = "//mat-cell/button[1]")
    private List<WebElement> status;
    @FindBy(how = How.CSS, css = ".mat-icon-button")
    private List<WebElement> actions;
    @FindBy(how = How.XPATH, xpath = "//mat-row/mat-cell[2]/a")
    private List<WebElement> guidelineTitles;

    List<String> titles;

    private static final String inputPatientJsonFolderPath = "payloads/";
    public String guidelineName;
    public int rowNumber;
    public WebElement guidelineActions;

    public GuidelinePage(WebDriver driver) {
        super(driver);
    }

    public void waitForStatusDisplay(String guidelineName) {
        int rowNumber = lastIndexOfGuidelines(guidelineName);
        String xpath = "//mat-table[@class='mat-table']//mat-row[" + (rowNumber + 1) + "]//mat-cell[7]//button[1]";
        Log.info("rowNumber: " + rowNumber);
        wait.forTextInElement(By.xpath(xpath), "Under Test", GUIDELINE_PUBLISH_TIMEOUT);
    }

    public void waitForGuidelineUploaded() {
        String guidelineInserted = "//span[contains(text(),'You have inserted 1 guideline.')]";
        wait.forElementPresent(By.xpath(guidelineInserted), GUIDELINE_UPLOAD_TIMEOUT);
        refreshPage();
    }

    public void onUploadAGuidelineJsonFile(String fileName) {
        guidelineName = fileName;
        String absoluteFilePath = getAbsoluteFilePath(inputPatientJsonFolderPath + fileName);
        jsonInput.sendKeys(absoluteFilePath);
        wait.forElementVisible(guidelineInserted,GUIDELINE_PUBLISH_TIMEOUT);
        Log.info("The Guideline is inserted");
    }

    public void onUploadInvalidGuidelineJsonFile(String fileName) {
        guidelineName = fileName;
        String absoluteFilePath = getAbsoluteFilePath(inputPatientJsonFolderPath + fileName);
        jsonInput.sendKeys(absoluteFilePath);
        wait.forElementVisible(guidelineRejected,GUIDELINE_PUBLISH_TIMEOUT);
        Log.info("The Guideline was rejected");
    }


    public boolean isGuidelineUploadButtonDisplay() {
        waitForPageLoad();
        Log.info("Waiting for guideline page in: " + getCurrentUrl());
        //wait.forElementVisible(createButton, GUIDELINE_PUBLISH_TIMEOUT);
        wait.forElementVisible(uploadButton, GUIDELINE_PUBLISH_TIMEOUT);
        return isElementDisplayed(uploadButton);
    }

    public List<String> getHeaders() {
        return headers.stream()
                .map(header -> header.getText())
                .collect(Collectors.toList());
    }

    public int lastIndexOfGuidelines(String name) {
        Log.info("Looking for guideline " + name);
        int index = getGuidelineNames().lastIndexOf(name);
        Log.info("Index: " + index);
        return index;
    }

    public List<String> getGuidelineNames() {
        return driver.findElements(By.xpath("//mat-row/mat-cell[2]/a")).stream()
            .map(WebElement::getText)
            .collect(Collectors.toList());
    }

    public WebElement getGuidelineCheckBoxElement(String fileName) {
        return checkboxs.get(lastIndexOfGuidelines(fileName) + 1);
    }

    public boolean verifyGuidelineIsCreated(String fileName) {
        guidelineName = fileName;
        refreshPage();
        Log.info("Check if the guideline is loaded");
        waitForElementVisible(guidelineTable);
        if (isElementOnPage(guidelineTable)) {
            Log.info("guidelineTable is present.");
            List<String> guidelineNames = getGuidelineNames();
            Log.info("The file expected is: " + fileName);
            Log.info("Guidelines loaded: " + guidelineNames);
            return guidelineNames.contains(fileName);
        }
        Log.info("table is not displayed.");
        return false;
    }

    public boolean setGuidelineStatusToUnderTest(String jsonfileName) {
        waitForPageLoad();
        guidelineName = jsonfileName;
        rowNumber = lastIndexOfGuidelines(jsonfileName);
        Log.info(jsonfileName + " row number is " + rowNumber);
        guidelineActions = actions.get(rowNumber);
        new Actions(driver)
                .moveToElement(guidelineActions)
                .click(guidelineActions)
                .build()
                .perform();
        Log.info("Action button is available");
        testButton.click();
        wait.forElementVisible(guidelineUnderTest,GUIDELINE_PUBLISH_TIMEOUT);
        if (isElementDisplayed(guidelineUnderTest)) {
            Log.info("Status changed to under test.");
            return true;
        }
        return false;
    }

    public boolean isGuidelineStatusIsUnderTest(String guidelineName) {
        rowNumber = lastIndexOfGuidelines(guidelineName);
        Log.info(guidelineName + " status is " + status.get(rowNumber).getText());
        if (status.get(rowNumber).getText().equals("Under Test")) {
            return true;
        }
        return false;
    }

    public boolean isGuidelineStatusIsDraft(String guidelineName) {
        rowNumber = lastIndexOfGuidelines(guidelineName);
        Log.info(guidelineName + " status is " + status.get(rowNumber).getText());
        if (status.get(rowNumber).getText().equals("Draft")) {
            return true;
        }
        return false;
    }
    public boolean setGuidelineStatusToDraft(String jsonfileName) {
        waitForPageLoad();
        //rowNumber = lastIndexOfGuidelines(jsonfileName);
        Log.info(jsonfileName + " row number is " + rowNumber);
        new Actions(driver)
                .moveToElement(actions.get(rowNumber))
                .click(actions.get(rowNumber))
                .build()
                .perform();
//        guidelineActions.click();
        if (isElementDisplayed(testButton)) { //testButton and Returntodraft have the same location
            testButton.click();
            Log.info("Action button clicked");
            wait.forElementVisible(guidelineDraft,GUIDELINE_PUBLISH_TIMEOUT);
            Log.info("Status changed to draft");
            return true;
        }
        return false;
    }

    public Boolean deleteUploadGuideline(String fileName) {
        Log.info("Delete a guideline (from Guideline page) " + driver.getCurrentUrl());
        changeImplicitWait(GUIDELINE_DELETE_TIMEOUT, TimeUnit.SECONDS);
        isChecked(getGuidelineCheckBoxElement(fileName));
        Log.info("Checked is completed");
        if (isElementDisplayed(deleteButton)) {
            deleteButton.click();
            wait.forElementVisible(guidelineDeleted, GUIDELINE_PUBLISH_TIMEOUT);
            Log.info("Guideline is deleted.");
            return true;
        }
        return false;
    }
}
