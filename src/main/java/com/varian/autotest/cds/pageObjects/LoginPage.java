package com.varian.autotest.cds.pageObjects;

import static com.varian.autotest.cds.Index.*;
import static com.varian.autotest.cds.managers.FileReaderManager.getConfigReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Optional;

import com.varian.autotest.cds.logging.Log;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePageObject {

    public static final String COOKIE_FILE = "Cookies.data";

    @FindBy(xpath = "//input[@id='username']")
    private WebElement emailInput;
    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;
    @FindBy(xpath = "//*[@class='btn btn-primary ng-binding']")
    private WebElement signInButton;

    @FindBy(xpath = "//*[@id='i0116']")
    private WebElement azureLogininput;

    @FindBy(xpath = "//*[@id='i0118']")
    private WebElement azurePasswordinput;

    @FindBy(xpath = "//*[@id='idSIButton9']")
    private WebElement azureNextButton;

    @FindBy(xpath = "//*[@id='idSIButton9']")
    private WebElement azureSignInButton;

    @FindBy(xpath = "//*[@id='idBtn_Back']")
    private WebElement azureStaySignInButtonNo;

    @FindBy(xpath = "//a[contains(text(),'VarianCorporateAzureAD')]")
    private WebElement vaisLoginButton;


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void openGuidelineListURL() {
        navigateTo(getPath(GUIDELINE_LIST));
    }

    public void openGuidelineUrlAzure() {
        navigateTo(getConfigReader().getDeploymentUrl());
        wait.forUrlContains("microsoft");
    }

    public void vaisLogin() {
        wait.waitForPageLoaded();
        vaisLoginButton.click();
        wait.forUrlContains("microsoftonline");
    }

    public void vaisLoginUsername() {
        wait.waitForPageLoaded();
        azureLogininput.sendKeys(getLoginName());
        azureNextButton.click();
        Log.info("Username entered");
    }

    public void vaisLoginPassword() {
        wait.waitForPageLoaded();
        azurePasswordinput.sendKeys(getLoginPassword());
        azureSignInButton.click();
        Log.info("Password entered");
    }

    public String openGuidelineUrl() {
        navigateTo(getConfigReader().getDeploymentUrl() + GUIDELINE_LIST);
        String mainTab = driver.getWindowHandle();
        return mainTab;
    }

    public String currentUrl() {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl;
    }

    public String azureRedirectUrlValue() {
        String attributeValue = getConfigReader().getAzureRedirectUrl();
        return attributeValue;
    }

    public String azureUrlValue() {
        String attributeValue = getConfigReader().getDeploymentUrl();
        return attributeValue;
    }

    public void signIn() {
        emailInput.sendKeys(getLoginName());
        passwordInput.sendKeys(getLoginPassword());
    }

    public void clickSigInButton() {
        wait.waitForPageLoaded();
        if(signInButton.getAttribute("type").equals("submit"))
            signInButton.click();
    }

    public Boolean isAzureNextButton(){
        if (isElementOnPage(azureNextButton)){
            return true;
        }
        return false;
    }

    public void azureSignIn() {
        wait.waitForPageLoaded();
        azureLogininput.sendKeys(getLoginName());
        azureSignInButton.click();
        Log.info("Login name entered");
    }

    public Boolean isAzureSignInButton(){
        if (isElementOnPage(azureSignInButton)){
            return true;
        }
        return false;
    }

    public void azurePassword() {
        wait.waitForPageLoaded();
        azurePasswordinput.sendKeys(getLoginPassword());
        azureSignInButton.click();
        Log.info("Password entered");
    }

    public void azureStaySignInButtonNo() {
        wait.waitForPageLoaded();
        azureStaySignInButtonNo.click();
        Log.info("Stay Sign In set to No");
    }

    public void deleteCookies(){
        File file = new File(COOKIE_FILE);
        // Delete old file if exists
        if(file.delete()){
            Log.info("Cookies file deleted");
        }
        else{
            Log.info("No Cookies file found");
        }
    }

    public void saveCookies() {
        File file = new File(COOKIE_FILE);
        try {
            file.createNewFile();
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                for(Cookie ck : driver.manage().getCookies()) {
                    bw.write(cookieToString(ck));
                    bw.newLine();
                }
            }
        } catch(Exception x) {
            Log.error("", x);
        }
    }

    String cookieToString(Cookie ck) {
        String line = String.join(";",
                ck.getName(),
                ck.getValue(),
                ck.getDomain(),
                ck.getPath(),
                Optional.ofNullable(ck.getExpiry()).map(Date::toString).orElse("null"),
                Boolean.toString(ck.isSecure()));
        return line;
    }
}
