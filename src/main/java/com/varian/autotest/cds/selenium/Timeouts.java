package com.varian.autotest.cds.selenium;

public final class Timeouts {

    public static final int GUIDELINE_UPLOAD_TIMEOUT = 120;
    public static final int GUIDELINE_PUBLISH_TIMEOUT = 180;
    public static final int GUIDELINE_DELETE_TIMEOUT = 20;
}
