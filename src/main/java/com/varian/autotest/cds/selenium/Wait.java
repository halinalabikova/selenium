package com.varian.autotest.cds.selenium;

import static org.openqa.selenium.support.ui.ExpectedConditions.urlContains;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.varian.autotest.cds.core.SelectorStack;
import com.varian.autotest.cds.core.exceptions.CommonExpectedConditions;
import com.varian.autotest.cds.logging.Log;

public class Wait {

    private static final int DEFAULT_TIMEOUT = 15;
    private static final int DEFAULT_SLEEP = 5000;
    private static final String INIT_MESSAGE = "INIT ELEMENT";

    private WebDriverWait wait;
    private WebDriverWait sleepingWait;
    private WebDriver driver;

    public Wait(WebDriver webDriver) {
        this.driver = webDriver;
        this.wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT);
        this.sleepingWait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT, DEFAULT_SLEEP);
    }

    public void forUrlContains(String text) {
        wait.until(urlContains(text));
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = driver -> ((JavascriptExecutor) driver)
                .executeScript("return document.readyState")
                .toString()
                .equals("complete");
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    public void waitForRecommendationsPage() {
        ExpectedCondition<Boolean> expectation = driver -> ((JavascriptExecutor) driver)
                .executeScript("return document.readyState")
                .toString()
                .equals("complete");
        try {
            Thread.sleep(5000);
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

    /**
     * Checks if the element is present in browser DOM
     */
    public WebElement forElementPresent(By by) {
        return forElementPresent(by, true);
    }

    public WebElement forElementPresent(By by, boolean failOnTimeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (TimeoutException e) {
            if (failOnTimeout) {
                Log.error(null, e);
            }
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementPresent(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver,
                    timeout
            ).until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (TimeoutException e) {
            Log.error(null, e);
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Checks if the element is clickable in browser
     */
    public WebElement forElementClickable(WebElement element) {
        changeImplicitWait(0, TimeUnit.MILLISECONDS);
        try {
            element.getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        try {
            if (SelectorStack.isContextSet()) {
                SelectorStack.contextRead();
                return wait.until(ExpectedConditions.elementToBeClickable(element));
            } else {
                return forElementClickable(SelectorStack.read());
            }
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(WebElement element, int timeout) {
        changeImplicitWait(0, TimeUnit.MILLISECONDS);
        try {
            element.getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        try {
            if (SelectorStack.isContextSet()) {
                SelectorStack.contextRead();
            }
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(
                    element));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(List<WebElement> elements, int index, int timeout) {
        changeImplicitWait(0, TimeUnit.MILLISECONDS);
        try {
            elements.get(index).getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        try {
            SelectorStack.contextRead();
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(
                    elements.get(index)));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Checks if the element is visible in browser
     */
    public WebElement forElementVisible(WebElement element) {
        changeImplicitWait(0, TimeUnit.MILLISECONDS);
        try {
            element.getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        if (SelectorStack.isContextSet()) {
            SelectorStack.contextRead();
            return wait.until(ExpectedConditions.visibilityOf(element));
        } else {
            return forElementVisible(SelectorStack.read());
        }
    }

    public WebElement forElementVisible(WebElement element, int timeoutSec, int polling) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeoutSec, polling).until(ExpectedConditions.visibilityOf(
                    element));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementVisible(WebElement element, int timeoutSec) {
        return forElementVisible(element, timeoutSec, 500);
    }

    public WebElement forElementVisible(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to be either invisible or not present on the DOM.
     */
    public boolean forElementNotVisible(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver,
                    DEFAULT_TIMEOUT
            ).until(ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forElementNotVisible(By by, int timeout, int polling) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver,
                    timeout,
                    polling
            ).until(ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forElementNotVisible(By by, Duration timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver,
                    timeout.getSeconds()
            ).until(ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to not be present in element attribute
     */

    public boolean forValueToBeNotPresentInElementsAttribute(
            WebElement element, String attribute, String value
    ) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.valueToBeNotPresentInElementsAttribute(element,
                    attribute,
                    value
            ));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to be present in element attribute
     */
    public boolean forValueToBePresentInElementsAttribute(
            WebElement element, String attribute, String value
    ) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.valueToBePresentInElementsAttribute(element,
                    attribute,
                    value
            ));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to not be present in DOM
     */
    public boolean forElementNotPresent(By selector) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.elementNotPresent(selector));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for text to not be present in DOM
     */
    public boolean forTextNotInElement(WebElement element, String text) {
        try {
            element.getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            if (SelectorStack.isContextSet()) {
                SelectorStack.contextRead();
                return wait.until(CommonExpectedConditions.textToBeNotPresentInElement(element, text));
            } else {
                return forTextNotInElement(SelectorStack.read(), text);
            }
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextNotInElement(By by, String text) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.textToBeNotPresentInElement(by, text));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for text to be present in DOM
     */
    public boolean forTextInElement(By by, String text, int seconds) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return new WebDriverWait(driver, seconds).until(CommonExpectedConditions.textToBePresentInElement(by, text));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextInElement(By by, String text) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.textToBePresentInElement(by, text));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextInElement(By by, int index, String text) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.textToBePresentInElement(by, index, text));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextInElement(WebElement element, String text) {
        try {
            element.getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            if (SelectorStack.isContextSet()) {
                SelectorStack.contextRead();
                return wait.until(CommonExpectedConditions.textToBePresentInElement(element, text));
            } else {
                return forTextInElement(SelectorStack.read(), text);
            }
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextInElement(List<WebElement> elements, int index, String text) {
        try {
            elements.get(0).getTagName();
        } catch (WebDriverException e) {
            Log.info(INIT_MESSAGE);
        }
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            if (SelectorStack.isContextSet()) {
                SelectorStack.contextRead();
                return wait.until(CommonExpectedConditions.textToBePresentInElement(elements, index, text));
            } else {
                return forTextInElement(SelectorStack.read(), index, text);
            }
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forTextInElementAfterRefresh(WebElement element, String text) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.textToBePresentInElementAfterRefresh(element,
                    text
            ));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for text in element to be present after refresh
     */
    public boolean forTextInElementAfterRefresh(By by, String text) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return sleepingWait.until(CommonExpectedConditions.textToBePresentInElementAfterRefresh(by,
                    text
            ));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for attribute in element to contain
     */
    public boolean forAttributeToContain(WebElement element, String attribute, String expectedValue) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.valueToBePresentInElementsAttribute(element,
                    attribute,
                    expectedValue
            ));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for attribute in element to be present
     */
    public boolean forAttributeToBePresent(WebElement element, String attribute) {
        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.attributeToBePresentInElement(element, attribute));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forAttributeInElement(WebElement elm, String attribute, String value) {

        changeImplicitWait(0, TimeUnit.SECONDS);
        try {
            return wait.until(CommonExpectedConditions.valueToBePresentInElementsAttribute(elm, attribute, value));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    private void restoreDefaultImplicitWait() {
        changeImplicitWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    private void changeImplicitWait(int value, TimeUnit timeUnit) {
        driver.manage().timeouts().implicitlyWait(value, timeUnit);
    }

    /**
     * Wait for fixed time
     */
    public void forXMilliseconds(int time) {
        forX(Duration.ofMillis(time));
    }

    public void forX(Duration duration) {
        Log.info("Wait for " + duration.toMillis() + " ms");
        try {
            Thread.sleep(duration.toMillis());
        } catch (InterruptedException e) {
            Log.error("Wait.forXMilliseconds", e);
        }
    }
}