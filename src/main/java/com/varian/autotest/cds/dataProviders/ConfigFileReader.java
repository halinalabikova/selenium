package com.varian.autotest.cds.dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import com.varian.autotest.cds.enums.DriverType;
import com.varian.autotest.cds.enums.EnvironmentType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigFileReader {

    private static Logger log = LogManager.getLogger(ConfigFileReader.class);

    private Properties properties;
    private final String propertyFilePath = "configs//Configuration.properties";

    public ConfigFileReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public String getDriverPath() {
        String driverPath = properties.getProperty("driverPath");
        if (driverPath != null) return driverPath;
        else
            throw new RuntimeException("Driver Path not specified in the Configuration.properties file for the Key:driverPath");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if (implicitlyWait != null) {
            try {
                return Long.parseLong(implicitlyWait);
            } catch (NumberFormatException e) {
                throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
            }
        }
        return 30;
    }

    public String getDeploymentUrl() {
        String url = properties.getProperty("deploymentUrl");
        log.info("Url: " + url);
        if (url != null) {
            return url;
        } else {
            throw new RuntimeException("Application Url not specified in the Configuration.properties file for the Key:url");
        }
    }

    public String getRedirectUrl() {
        String url = properties.getProperty("redirectUrl");
        if (url != null) {
            return url;
        } else {
            throw new RuntimeException("Redirection Url not specified in the Configuration.properties file for the Key:url");
        }
    }

    public String getAzureRedirectUrl() {
        String url = properties.getProperty("azureRedirectUrl");
        if (url != null) {
            return url;
        } else {
            throw new RuntimeException("Azure Redirection Url not specified in the Configuration.properties file for the Key:azureRedirectUrl");
        }
    }

    public String getUsername() {
        String loginName = properties.getProperty("username");
        if (loginName != null) {
            return loginName;
        } else {
            throw new RuntimeException("Login username is  not specified in the Configuration.properties file for the Key:loginName");
        }
    }

    public String getPassword() {
        String loginPWD = properties.getProperty("password");
        if (loginPWD != null) {
            return loginPWD;
        } else {
            throw new RuntimeException("Login password is  not specified in the Configuration.properties file for the Key:loginPWD");
        }
    }

    public DriverType getBrowser() {
        String browserName = properties.getProperty("browser");
        if (browserName == null || browserName.equals("chrome")) return DriverType.CHROME;
        else if (browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX;
        else if (browserName.equals("iexplorer")) return DriverType.INTERNETEXPLORER;
        else
            throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName);
    }

    public EnvironmentType getEnvironment() {
        String environmentName = properties.getProperty("environment");
        if (environmentName == null || environmentName.equalsIgnoreCase("local")) return EnvironmentType.LOCAL;
        else if (environmentName.equals("remote")) return EnvironmentType.REMOTE;
        else
            throw new RuntimeException("Environment Type Key value in Configuration.properties is not matched : " + environmentName);
    }

    public Boolean getBrowserWindowMaximize() {
        String windowSize = properties.getProperty("windowMaximize");
        if (windowSize != null) return Boolean.valueOf(windowSize);
        return true;
    }

    public String getTestDataResourcePath() {
        String testDataResourcePath = properties.getProperty("testDataResourcePath");
        if (testDataResourcePath != null) return testDataResourcePath;
        else
            throw new RuntimeException("Test Data Resource Path not specified in the Configuration.properties file for the Key:testDataResourcePath");
    }

    public String getReportConfigPath() {
        String reportConfigPath = properties.getProperty("reportConfigPath");
        if (reportConfigPath != null) return reportConfigPath;
        else
            throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
    }

    public boolean isHeadless() {
        String headless = properties.getProperty("headless");
        return Boolean.parseBoolean(headless);

    }

    public String getWebdriverversion() {
        String webdriverVersion = properties.getProperty("webdriverversion");
        if (webdriverVersion != null) return webdriverVersion;
        else
            throw new RuntimeException("WebDriver version not specified in the Configuration.properties file for the Key:webdriverVersion");
    }

    public boolean getPipeline() {
        String pipeline = properties.getProperty("pipeline");
        return Boolean.parseBoolean(pipeline);
    }
}
