package com.varian.autotest.cds.utils;

import static org.testng.AssertJUnit.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.varian.autotest.cds.logging.Log;


public class TestJsonData {

    public static String getAbsoluteFilePath(String filePath) {
        String jsonFilePatch = filePath.endsWith(".json")?filePath:filePath+".json";
        Log.info("This is file path: " + jsonFilePatch);
        ClassLoader classLoader = TestJsonData.class.getClassLoader();
        File file = new File(classLoader.getResource(jsonFilePatch).getFile());
        return file.getAbsolutePath();
    }

    public static String getFileContent(String filePath) throws IOException {
        String content = "";
        try (InputStream is = new FileInputStream(getAbsoluteFilePath(filePath))) {
            content = IOUtils.toString(is, StandardCharsets.UTF_8);
        }
        return content;
    }

    public static String getResource(String filePath) {
        try {
            return getFileContent(filePath);
        } catch (IOException e) {
            Log.error("", e);
            return null;
        }
    }

    public static List<String> getResources(List<String> filePaths) {
        return filePaths.stream()
                .map(TestJsonData::getResource)
                .collect(Collectors.toList());
    }

    public static String getResourcesAsJsonArray(List<String> filePaths) {
        String joined = getResources(filePaths).stream()
                .collect(Collectors.joining(","));
        return "[" + joined + "]";
    }

    public static String getResourcesAsJsonArrayData(List<String> filePaths) {
        String joined = getResources(filePaths).stream()
                .collect(Collectors.joining(","));
        return joined ;
    }


    public static HttpResponse SendPostRequest(String url, String payload, String cookie) {
        try {

            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = new HttpPost(url);

            StringEntity entity = new StringEntity(payload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Cookie", cookie);

            Log.info("send POST to " + url);

            HttpResponse response = client.execute(httpPost);
            assertEquals(200, response.getStatusLine().getStatusCode());

            return response;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HttpResponse SendGetRequest (String url, String cookie){
        try {

            HttpClient client = HttpClientBuilder.create().build();
            HttpGet httpGet = new HttpGet(url);

            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("Cookie", cookie);

            Log.info("send GET to " + url);

            HttpResponse response = client.execute(httpGet);
            assertEquals(200, response.getStatusLine().getStatusCode());

            return response;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String modifyValueInJson(String key, String newValue, String json) {
        JsonElement root = new JsonParser().parse(json);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null && entry.getValue().isJsonPrimitive()) {
            entry.setValue(new JsonPrimitive(newValue));
        }
        return root.toString();
    }

    public static String setJsonValue(String key, String value, String json) {
        JsonElement root = new JsonParser().parse(json);
        JsonElement jsonValue = new JsonParser().parse(value);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null && entry.getValue().isJsonArray()) {
            entry.setValue(jsonValue);
        }
        return root.toString();
    }

    public static String setJsonObjectValue(String key, String value, String json) {
        JsonElement root = new JsonParser().parse(json);
        JsonElement jsonValue = new JsonParser().parse(value);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null) {
            entry.setValue(jsonValue);
        }
        return root.toString();
    }

    public static String addValueInJson(String key, String newValue, String json) {
        JsonElement root = new JsonParser().parse(json);
        JsonElement newValueJson = new JsonParser().parse(newValue);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null && entry.getValue().isJsonArray()) {
            entry.getValue().getAsJsonArray().add(newValueJson);
        }
        return root.toString();
    }

    public static Entry<String, JsonElement> findKeyInJson(String key, JsonElement root) {
        if (root.isJsonObject()) {
            JsonObject obj = root.getAsJsonObject();
            for (Entry<String, JsonElement> entry : obj.entrySet()) {
                String k = entry.getKey();
                if (k.equals(key)) {
                    return entry;
                }

                JsonElement v = entry.getValue();
                if (v.isJsonArray() || v.isJsonObject()) {
                    Entry<String, JsonElement> result = findKeyInJson(key, v);
                    if (result != null) {
                        return result;
                    }
                }
            }
        } else if (root.isJsonArray()) {
            JsonArray a = root.getAsJsonArray();
            for (JsonElement element : a) {
                Entry<String, JsonElement> result = findKeyInJson(key, element);
                if (result != null) {
                    return result;
                }
            }
        }

        return null;
    }

    public static String fetchValueInJson(String json, String parentKey, String childKey) {
        JsonObject root = new JsonParser().parse(json).getAsJsonObject();
        System.out.println("Patient Record: " + json);
        System.out.println("ParentKey: " + parentKey);
        System.out.println("ChildKey: " + childKey);
        String entry = fetchValueInJsonArray("valueQuantity",json);

        //JsonElement root = new JsonParser().parse(json);
//        Entry<String, JsonElement> entry = findKeyInJson(key, root);
//        if (entry != null) {
//            return entry.getValue().getAsString();
//        }
        return root.toString();
    }

    public static String fetchValueInJson(String key, String json) {
        JsonElement root = new JsonParser().parse(json);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null) {
            return entry.getValue().getAsString();
        }
        return root.toString();
    }

    public static String fetchValueInJsonArray(String key, String json) {
        JsonElement root = new JsonParser().parse(json);
        Entry<String, JsonElement> entry = findKeyInJson(key, root);
        if (entry != null) {
            return entry.getValue().toString();
        }
        return root.toString();
    }

    public static String generateRandomNDigitString(int n) {
        return UUID.randomUUID().toString().substring(36 - n);
    }
}
